"""
Bar chart demo with pairs of bars grouped for easy comparison.
"""
import numpy as np
import matplotlib.pyplot as plt
import math
import networkx as nx
from gurobipy import *
import scipy.special


def boxplot():
    n_groups = 5

    means_men = (20, 35, 30, 35, 27)
    std_men = (2, 3, 4, 1, 2)

    means_women = (25, 32, 34, 20, 25)
    std_women = (3, 5, 2, 3, 3)

    index = np.arange(n_groups)
    plt.bar(index, means_men)
    plt.axhline(y=35, color='r', linestyle='--')
    plt.xlabel('Group')
    plt.ylabel('Scores')
    plt.title('Scores by group and gender')
    plt.xticks(index, ['A', 'B', 'C', 'D', 'E'])

    plt.tight_layout()
    plt.show()


def append_file():
    fobj = open('blub.txt', "r")
    fobj.readline()
    fobj.close()


def knotenanzahl(k, m):
    return int(-0.5 + math.sqrt(0.25 + 2 * k/m))+1


def kantenanzahl(n, m):
    return int(m * n * (n-1)/2)


def edge_index():
    graph = nx.Graph()
    graph.add_edge(1, 2, index=1)
    graph.add_edge(2, 3, index=2)
    graph.add_edge(1, 3, index=3)

    ei = nx.get_edge_attributes(graph, 'index')

    eis = {}
    eis[1] = (1,2)
    print(eis)


# for i in range(7, 15, 1):
#     print(i, kantenanzahl(i,1))
#
# print()
# for i in range(9, 18, 1):
#     print(i, kantenanzahl(i,2/3))

print(list(range(30, 91, 10)))
