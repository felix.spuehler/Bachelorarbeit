import sys
sys.path.insert(0, '../src/')

import create_blocks as cb
import evaluation as eva
import create_ksp as cdk
import create_mst as cdm
from datetime import datetime as dt
import knapsack as ks

number_of_problems = 10
number_of_nodes = 173
number_of_edges = 9918
problem_info = [number_of_nodes, number_of_edges]

blocks, block_info = cb.misch(number_of_edges, 3, 2)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)

blocks, block_info = cb.misch(number_of_edges, 2, 3)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)