import sys
sys.path.insert(0, '../src/')

import create_blocks as cb
import evaluation as eva
import create_mst as cdm
from datetime import datetime as dt

number_of_problems = 10
number_of_nodes = 173
number_of_edges = 9918
problem_info = [number_of_nodes, number_of_edges]
number_of_elements = number_of_edges

blocks, block_info = cb.ueberlapp(number_of_elements, 2)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.ueberlapp_rev(number_of_elements, 2)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)

blocks, block_info = cb.ueberlapp(number_of_elements, 3)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.ueberlapp_rev(number_of_elements, 3)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)

blocks, block_info = cb.ueberlapp(number_of_elements, 4)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.ueberlapp_rev(number_of_elements, 4)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)

blocks, block_info = cb.ueberlapp(number_of_elements, 100)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.ueberlapp_rev(number_of_elements, 100)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)

##########################################################################################

blocks, block_info = cb.ueberlapp_mod(number_of_elements, 2)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.ueberlapp_mod_rev(number_of_elements, 2)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)

blocks, block_info = cb.ueberlapp_mod(number_of_elements, 3)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.ueberlapp_mod_rev(number_of_elements, 3)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)

blocks, block_info = cb.ueberlapp_mod(number_of_elements, 4)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.ueberlapp_mod_rev(number_of_elements, 4)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)

blocks, block_info = cb.ueberlapp_mod(number_of_elements, 100)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.ueberlapp_mod_rev(number_of_elements, 100)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)

##########################################################################################

blocks, block_info = cb.disjunkt(number_of_elements, 2)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.disjunkt_rev(number_of_elements, 2)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)

blocks, block_info = cb.disjunkt(number_of_elements, 3)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.disjunkt_rev(number_of_elements, 3)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)

blocks, block_info = cb.disjunkt(number_of_elements, 4)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.disjunkt_rev(number_of_elements, 4)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)

blocks, block_info = cb.disjunkt(number_of_elements, 100)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.disjunkt_rev(number_of_elements, 100)
eva.calculating_one_block_set('mst', number_of_problems, problem_info, blocks, block_info)