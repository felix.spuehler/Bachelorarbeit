import sys
sys.path.insert(0, '../src/')

import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

import scipy.special
import numpy as np

import create_blocks as cb
import evaluation as eva


def plot_loop_values(problem_name, number_of_problems, problem_info, block_name):
    opt_val, aver = eva.read_optimal_solutions(problem_name, problem_info)

    file = open('../sol/{0}/{1}_{2}_{3}.eva'.format(problem_name, problem_info[0], problem_info[1], block_name), 'r')

    for i in range(14):
        file.readline()

    fig = plt.figure(figsize=(15, 8))
    ax = fig.gca()
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    plt.xlabel('Anzahl Runden')
    plt.ylabel('Wert')

    p = {}
    for i in range(number_of_problems):
        file.readline()
        line = file.readline()
        loop_values = [float(x) for x in line.split('; ')]
        p[i] = plt.plot(list(range(len(loop_values))), loop_values, '-o', label='Problem ' + str(i+1))
        plt.plot([len(loop_values)-1], [opt_val[i]], marker='x', markersize=10, color=p[i][0].get_color())
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.grid(True)
    handles, labels = ax.get_legend_handles_labels()
    lgd = ax.legend(handles, labels, loc='center left', bbox_to_anchor=(1, 0.5))
    fig.savefig('../sol/{0}/pics/{1}_{2}_{3}.png'.format(problem_name, problem_info[0], problem_info[1], block_name), bbox_extra_artists=(lgd,), bbox_inches='tight')

    plt.close()


def plot_all_k_subs(problem_name, problem_info):
    if problem_name == 'ksp':
        number_of_elements = problem_info[0]
    else:  # problem_name == 'mst'
        number_of_elements = problem_info[1]
    
    # read values
    file = open('../sol/{0}/{1}_{2}_all-k-subs.eva'.format(problem_name, problem_info[0], problem_info[1]), 'r')
    for i in range(12):
        file.readline()

    res_k = []
    res_sol = []
    res_loops = []
    res_gap = []
    res_time = []
    res_rel = []
    for i in range(number_of_elements):
        k, sol, loops, gap, time, freq = file.readline().split('; ')
        res_k.append(int(k))
        res_sol.append(float(sol))
        res_loops.append(float(loops))
        res_gap.append(float(gap))
        res_time.append(float(time))
        res_rel.append(float(freq))
    file.close()

    res_k = res_k[1:]
    res_sol = res_sol[1:]
    res_loops = res_loops[1:]
    res_gap = res_gap[1:]
    res_time = res_time[1:]
    res_rel = res_rel[1:]

    '''start plotting'''
    # solution
    plt.plot(res_k, res_sol, '-o')
    plt.axhline(y=res_sol[-1], color='r', linestyle='--', label='optimal solution')
    plt.xticks(res_k, res_k)
    plt.xlabel('k-elementige Teilmengen')
    plt.ylabel('Wert')
    plt.gca().yaxis.grid(True)
    plt.legend()
    plt.savefig('../sol/{0}/pics/{1}_{2}_all-k-subs_sol.png'.format(problem_name, problem_info[0], problem_info[1]), bbox_inches='tight')
    plt.close()

    # loops
    plt.plot(res_k, res_loops, '-o')
    plt.xticks(res_k, res_k)
    plt.xlabel('k-elementige Teilmengen')
    plt.ylabel('Anzahl Runden')
    plt.gca().yaxis.grid(True)
    plt.savefig('../sol/{0}/pics/{1}_{2}_all-k-subs_loops.png'.format(problem_name, problem_info[0], problem_info[1]), bbox_inches='tight')
    plt.close()

    # gap
    plt.plot(res_k, res_gap, '-o')
    plt.xticks(res_k, res_k)
    plt.xlabel('k-elementige Teilmengen')
    plt.ylabel('Gap')
    plt.gca().yaxis.grid(True)
    plt.savefig('../sol/{0}/pics/{1}_{2}_all-k-subs_gap.png'.format(problem_name, problem_info[0], problem_info[1]), bbox_inches='tight')
    plt.close()

    # time
    plt.plot(res_k, [i / 60 for i in res_time], '-o')
    plt.xticks(res_k, res_k)
    plt.xlabel('k-elementige Teilmengen')
    plt.ylabel('Zeit in Minuten')
    plt.gca().yaxis.grid(True)
    plt.savefig('../sol/{0}/pics/{1}_{2}_all-k-subs_time.png'.format(problem_name, problem_info[0], problem_info[1]), bbox_inches='tight')
    plt.close()

    # freq
    index = range(number_of_elements-1)
    plt.bar(index, res_rel)
    plt.xlabel('k-elementige Teilmengen')
    plt.ylabel('Relative Häufigkeit')
    plt.xticks(index, range(1, number_of_elements + 1))
    plt.gca().yaxis.grid(True)
    plt.tight_layout()
    plt.savefig('../sol/{0}/pics/{1}_{2}_all-k-subs_freq.png'.format(problem_name, problem_info[0], problem_info[1]), bbox_inches='tight')
    plt.close()

    plt.plot(res_k, res_rel, '-o')
    plt.xticks(res_k, res_k)
    plt.xlabel('k-elementige Teilmengen')
    plt.ylabel('Relative Häufigkeit')
    plt.gca().yaxis.grid(True)
    plt.savefig('../sol/{0}/pics/{1}_{2}_all-k-subs_freq2.png'.format(problem_name, problem_info[0], problem_info[1]), bbox_inches='tight')
    plt.close()

    # number of subsets
    plt.plot(res_k, [scipy.special.binom(number_of_elements, i) for i in res_k], '-o')
    plt.xticks(res_k, res_k)
    plt.xlabel('k-elementige Teilmengen')
    plt.ylabel('Anzahl Blöcke')
    plt.gca().yaxis.grid(True)
    plt.savefig('../sol/{0}/pics/{1}_{2}_all-k-subs_nos.png'.format(problem_name, problem_info[0], problem_info[1]), bbox_inches='tight')
    plt.close()

    # comparison time and nos
    fig, ax1 = plt.subplots()
    plt.legend()
    ax1.plot(res_k, [i / 60 for i in res_time], 'b-o', label='Zeit')
    plt.xticks(res_k, res_k)
    ax1.set_xlabel('k-elementige Teilmengen')
    ax1.set_ylabel('Zeit in Minuten')
    plt.gca().yaxis.grid(True)
    ax2 = ax1.twinx()
    ax2.plot(res_k, [scipy.special.binom(number_of_elements, i) for i in res_k], 'r--o', label='Blockanzahl')
    ax2.set_ylabel('Anzahl Blöcke')
    ax2.plot(np.nan, np.nan, 'b-o', label='Zeit')
    plt.legend()
    plt.savefig('../sol/{0}/pics/{1}_{2}_all-k-subs_time_nos.png'.format(problem_name, problem_info[0], problem_info[1]), bbox_inches='tight')
    plt.close()


def plot_time_sol(problem_name, problem_info, auswahl = False):
    subsets_list = cb.read_block_list_special(problem_name, problem_info, 'time_sol')
    file_beginning = '../sol/{0}/{1}_{2}_'.format(problem_name, problem_info[0], problem_info[1])

    # read values
    sol = []
    time = []

    for name in subsets_list:
        if not name.find('zufall') == -1:
            continue
        file = open(file_beginning + name + '.eva', 'r')
        for i in range(11):
            file.readline()

        s, l, g, t = file.readline().split('; ')
        sol.append(float(s))
        time.append(float(t)/60)
        file.close()

    opt_val, aver = eva.read_optimal_solutions(problem_name, problem_info)

    '''start plotting'''
    # set colors
    cm = plt.get_cmap('gist_rainbow')
    colors = [cm(1. * i / len(subsets_list)) for i in range(len(subsets_list))]

    # plotting
    fig = plt.figure()
    ax = plt.subplot(111)
    ax.set_prop_cycle('color', colors)

    for i, name in enumerate(subsets_list):
        if not name.find('zufall') == -1:
            continue
        x = time[i]
        y = sol[i]
        plt.scatter(x, y, label=name, alpha=0.7)

    plt.axhline(y=aver, color='r', linestyle='--', label='Optimaler Wert')

    plt.xlabel('Zeit in Minuten')
    plt.ylabel('Wert')
    handles, labels = ax.get_legend_handles_labels()
    lgd = ax.legend(handles, labels, loc='center left', bbox_to_anchor=(1, 0.5))
    ax.grid(True)
    if auswahl:
        fig.savefig('../sol/{0}/pics/{1}_{2}_time_sol_2.png'.format(problem_name, problem_info[0], problem_info[1]), bbox_extra_artists=(lgd,), bbox_inches='tight')
    else:
        fig.savefig('../sol/{0}/pics/{1}_{2}_time_sol.png'.format(problem_name, problem_info[0], problem_info[1]), bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.close()


def plot_nos_time(problem_name, problem_info, auswahl = False):
    subsets_list = cb.read_block_list_special(problem_name, problem_info, 'nos_time')
    file_beginning = '../sol/{0}/{1}_{2}_'.format(problem_name, problem_info[0], problem_info[1])
    
    # read values
    number_of_blocks = []
    time = []

    for name in subsets_list:
        if not name.find('zufall') == -1:
            continue
        file = open(file_beginning + name + '.eva', 'r')

        file.readline()
        t, n = file.readline().split('; ')
        number_of_blocks.append(int(n))
        for i in range(9):
            file.readline()
        s, l, g, t = file.readline().split('; ')
        time.append(float(t)/60)
        file.close()

    '''start plotting'''
    # set colors
    cm = plt.get_cmap('gist_rainbow')
    colors = [cm(1. * i / len(subsets_list)) for i in range(len(subsets_list))]

    # plotting
    fig = plt.figure()
    ax = plt.subplot(111)
    ax.set_prop_cycle('color', colors)

    for i, name in enumerate(subsets_list):
        if not name.find('zufall') == -1:
            continue
        x = number_of_blocks[i]
        y = time[i]
        plt.scatter(x, y, label=name, alpha=0.7)

    plt.xlabel('Anzahl Blöcke')
    plt.ylabel('Zeit in Minuten')
    handles, labels = ax.get_legend_handles_labels()
    lgd = ax.legend(handles, labels, loc='center left', bbox_to_anchor=(1, 0.5))
    ax.grid(True)
    if auswahl:
        fig.savefig('../sol/{0}/pics/{1}_{2}_nos_time_2.png'.format(problem_name, problem_info[0], problem_info[1]), bbox_extra_artists=(lgd,), bbox_inches='tight')
    else:
        fig.savefig('../sol/{0}/pics/{1}_{2}_nos_time.png'.format(problem_name, problem_info[0], problem_info[1]), bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.close()


def plot_nos_loops(problem_name, problem_info, auswahl = False):
    subsets_list = cb.read_block_list_special(problem_name, problem_info, 'nos_loops')
    file_beginning = '../sol/{0}/{1}_{2}_'.format(problem_name, problem_info[0], problem_info[1])

    # read values
    number_of_blocks = []
    loops = []

    for name in subsets_list:
        if not name.find('zufall') == -1:
            continue

        file = open(file_beginning + name + '.eva', 'r')

        file.readline()
        t, n = file.readline().split('; ')
        number_of_blocks.append(int(n))
        for i in range(9):
            file.readline()
        s, l, g, t = file.readline().split('; ')
        loops.append(float(l))
        file.close()

    '''start plotting'''
    # set colors
    cm = plt.get_cmap('gist_rainbow')
    colors = [cm(1. * i / len(subsets_list)) for i in range(len(subsets_list))]

    # plotting
    fig = plt.figure()
    ax = plt.subplot(111)
    ax.set_prop_cycle('color', colors)

    for i, name in enumerate(subsets_list):
        if not name.find('zufall') == -1:
            continue
        x = number_of_blocks[i]
        y = loops[i]
        plt.scatter(x, y, label=name, alpha=0.7)

    plt.xlabel('Anzahl Blöcke')
    plt.ylabel('Anzahl Runden')
    handles, labels = ax.get_legend_handles_labels()
    lgd = ax.legend(handles, labels, loc='center left', bbox_to_anchor=(1, 0.5))
    ax.grid(True)
    if auswahl:
        fig.savefig('../sol/{0}/pics/{1}_{2}_nos_loops_2.png'.format(problem_name, problem_info[0], problem_info[1]), bbox_extra_artists=(lgd,), bbox_inches='tight')
    else:
        fig.savefig('../sol/{0}/pics/{1}_{2}_nos_loops.png'.format(problem_name, problem_info[0], problem_info[1]), bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.close()


def plot_comparison(problem_name, problem_info):
    subsets_list = cb.read_block_list(problem_name, problem_info)

    file_beginning = '../sol/{0}/{1}_{2}_'.format(problem_name, problem_info[0], problem_info[1])

    # read values
    count_zufall = 0
    sol = []
    loops = []
    gap = []
    time = []
    freq = []
    number_of_blocks = []

    for name in subsets_list:
        if name.find('zufall') == -1 and count_zufall > 0:
            raise Exception('Zufall als letztes')
        elif not name.find('zufall') == -1:
            count_zufall += 1

        file = open(file_beginning + name + '.eva', 'r')

        file.readline()
        t, n = file.readline().split('; ')
        number_of_blocks.append(float(n))
        for i in range(7):
            file.readline()
        n, f = file.readline().split('; ')
        freq.append(float(f))

        file.readline()
        s, l, g, t = file.readline().split('; ')
        sol.append(float(s))
        loops.append(float(l))
        gap.append(float(g))
        time.append(float(t)/60)

        file.close()

    opt_val, aver = eva.read_optimal_solutions(problem_name, problem_info)

    # start plotting
    file_beginning = '../sol/{0}/pics/{1}_{2}_'.format(problem_name, problem_info[0], problem_info[1])
    plt.rc('axes', axisbelow=True)

    # plot sol
    index = range(len(subsets_list))
    plt.bar(index, sol)
    plt.axhline(y=aver, color='r', linestyle='--', label='Optimaler Wert')
    plt.xlabel('Blockmenge')
    plt.ylabel('Wert')
    plt.xticks(index, subsets_list, rotation=90)
    if problem_name == 'ksp':
        plt.legend(loc='lower right')
    else:
        plt.legend(loc='upper right')
    plt.gca().yaxis.grid(True)
    plt.tight_layout()
    plt.savefig(file_beginning + 'comparison_sol.png', bbox_inches='tight')
    plt.close()

    # plot loops
    if count_zufall == 0:
        index = range(len(subsets_list))
        plt.bar(index, loops)
        plt.xticks(index, subsets_list, rotation=90)
    else:
        index = range(len(subsets_list[:-count_zufall]))
        plt.bar(index, loops[:-count_zufall])
        plt.xticks(index, subsets_list[:-count_zufall], rotation=90)
    plt.xlabel('Blockmenge')
    plt.ylabel('Anzahl Runden')
    plt.gca().yaxis.grid(True)
    plt.tight_layout()
    plt.savefig(file_beginning + 'comparison_loops.png', bbox_inches='tight')
    plt.close()

    # plot gap
    index = range(len(subsets_list))
    plt.bar(index, gap)
    plt.xlabel('Blockmenge')
    plt.ylabel('Gap')
    plt.xticks(index, subsets_list, rotation=90)
    plt.gca().yaxis.grid(True)
    plt.tight_layout()
    plt.savefig(file_beginning + 'comparison_gap.png', bbox_inches='tight')
    plt.close()

    # plot time
    index = range(len(subsets_list))
    plt.bar(index, time)
    plt.xlabel('Blockmenge')
    plt.ylabel('Zeit in Minuten')
    plt.xticks(index, subsets_list, rotation=90)
    plt.gca().yaxis.grid(True)
    plt.tight_layout()
    plt.savefig(file_beginning + 'comparison_time.png', bbox_inches='tight')
    plt.close()

    # plot freq
    index = range(len(subsets_list))
    plt.bar(index, freq)
    plt.xlabel('Blockmenge')
    plt.ylabel('Relative Häufigkeit')
    plt.xticks(index, subsets_list, rotation=90)
    plt.gca().yaxis.grid(True)
    plt.tight_layout()
    plt.savefig(file_beginning + 'comparison_freq.png', bbox_inches='tight')
    plt.close()

    # plot number of subsets
    if count_zufall == 0:
        index = range(len(subsets_list))
        plt.bar(index, number_of_blocks)
        plt.xticks(index, subsets_list, rotation=90)
    else:
        index = range(len(subsets_list[:-count_zufall]))
        plt.bar(index, number_of_blocks[:-count_zufall])
        plt.xticks(index, subsets_list[:-count_zufall], rotation=90)
    plt.xlabel('Blockmenge')
    plt.ylabel('Anzahl Blöcke')
    plt.gca().yaxis.grid(True)
    plt.tight_layout()
    plt.savefig(file_beginning + 'comparison_nos.png', bbox_inches='tight')
    plt.close()


def plot_comparison_sample(problem_name, problem_info, sample):
    subsets_list = cb.read_block_list_special(problem_name, problem_info, '{0}'.format(sample))

    file_beginning = '../sol/{0}/{1}_{2}_'.format(problem_name, problem_info[0], problem_info[1])

    # read values
    sol = []
    loops = []
    gap = []
    time = []
    freq = []
    number_of_blocks = []

    for name in subsets_list:
        if not name.find('zufall') == -1:
            raise Exception('hier keine zufall - Auswertungen')

        file = open(file_beginning + name + '.eva', 'r')

        file.readline()
        t, n = file.readline().split('; ')
        number_of_blocks.append(int(n))
        for i in range(7):
            file.readline()
        n, f = file.readline().split('; ')
        freq.append(float(f))

        file.readline()
        s, l, g, t = file.readline().split('; ')
        sol.append(float(s))
        loops.append(float(l))
        gap.append(float(g))
        time.append(float(t)/60)

        file.close()

    opt_val, aver = eva.read_optimal_solutions(problem_name, problem_info)

    # start plotting
    file_beginning = '../sol/{0}/pics/{1}_{2}_comparison_{3}_'.format(problem_name, problem_info[0], problem_info[1], sample)
    plt.rc('axes', axisbelow=True)
    # plot sol
    index = range(len(subsets_list))
    plt.bar(index, sol)
    plt.axhline(y=aver, color='r', linestyle='--', label='Optimaler Wert')
    plt.xlabel('Blockmenge')
    plt.ylabel('Wert')
    plt.xticks(index, subsets_list, rotation=90)
    plt.legend(loc='lower right')
    plt.gca().yaxis.grid(True)
    plt.gca().get_yaxis().get_major_formatter().set_scientific(False)  # keine Ahnung, warum das nicht will...
    plt.savefig(file_beginning + 'sol.png', bbox_inches='tight')
    plt.close()

    # plot loops
    index = range(len(subsets_list))
    plt.bar(index, loops)
    plt.xlabel('Blockmenge')
    plt.ylabel('Anzahl Runden')
    plt.xticks(index, subsets_list, rotation=90)
    plt.gca().yaxis.grid(True)
    plt.tight_layout()
    plt.savefig(file_beginning + 'loops.png', bbox_inches='tight')
    plt.close()

    # plot gap
    index = range(len(subsets_list))
    plt.bar(index, gap)
    plt.xlabel('Blockmenge')
    plt.ylabel('Gap')
    plt.xticks(index, subsets_list, rotation=90)
    plt.gca().yaxis.grid(True)
    plt.tight_layout()
    plt.savefig(file_beginning + 'gap.png', bbox_inches='tight')
    plt.close()

    # plot time
    index = range(len(subsets_list))
    plt.bar(index, time)
    plt.xlabel('Blockmenge')
    plt.ylabel('Zeit in Minuten')
    plt.xticks(index, subsets_list, rotation=90)
    plt.gca().yaxis.grid(True)
    plt.tight_layout()
    plt.savefig(file_beginning + 'time.png', bbox_inches='tight')
    plt.close()

    # plot freq
    index = range(len(subsets_list))
    plt.bar(index, freq)
    plt.xlabel('Blockmenge')
    plt.ylabel('Relative Häufigkeit')
    plt.xticks(index, subsets_list, rotation=90)
    plt.gca().yaxis.grid(True)
    plt.tight_layout()
    plt.savefig(file_beginning + 'freq.png', bbox_inches='tight')
    plt.close()

    # plot number of subsets
    index = range(len(subsets_list))
    plt.bar(index, number_of_blocks)
    plt.xlabel('Blockmenge')
    plt.ylabel('Anzahl Blöcke')
    plt.xticks(index, subsets_list, rotation=90)
    plt.gca().yaxis.grid(True)
    plt.tight_layout()
    plt.savefig(file_beginning + 'nos.png', bbox_inches='tight')
    plt.close()


def plot_k_el_subs(problem_name, k, start_size, end_size, step_size, complete_graph=False):

    # read file
    if problem_name == 'ksp':
        file = open('../sol/ksp/teilmengen({0})_{1}_{2}_{3}.eva'.format(k, start_size, end_size, step_size), 'r')
    else:
        file = open('../sol/mst/teilmengen({0})_{1}_{2}_{3}_{4}.eva'.format(k, complete_graph, start_size, end_size, step_size), 'r')

    for i in range(10):
        file.readline()

    p_size = []
    loops = []
    gap = []
    time = []
    freq = []

    for ps in range(start_size, end_size + 1, step_size):
        p, s, l, g, t, f = file.readline().split('; ')

        p_size.append(ps)
        loops.append(float(l))
        gap.append(float(g))
        time.append(float(t) / 60)
        freq.append(float(f))

    file.close()

    # for mst add number of edges to p_size
    if problem_name == 'mst' and complete_graph:
        for i, p in enumerate(p_size):
            n = int(p * (p - 1) / 2)
            p_size[i] = '{0}\n{1}'.format(p, n)
    elif problem_name == 'mst' and not complete_graph:
        for i, p in enumerate(p_size):
            n = int(p * (p - 1) / 2)
            n = int(n * 2 / 3)
            p_size[i] = '{0}\n{1}'.format(p, n)

    '''start plotting'''
    plt.rc('axes', axisbelow=True)
    # plot loops
    index = range(len(p_size))
    plt.plot(index, loops)
    plt.xticks(index, p_size)
    plt.grid(True)
    plt.xlabel('Problemgröße')
    plt.ylabel('Anzahl Runden')
    if problem_name == 'ksp':
        plt.savefig('../sol/ksp/pics/teilmengen({0})_{1}_{2}_{3}_loops.png'.format(k, start_size, end_size, step_size), bbox_inches='tight')
    else:
        plt.savefig('../sol/mst/pics/teilmengen({0})_{1}_{2}_{3}_{4}_loops.png'.format(k, complete_graph, start_size, end_size, step_size), bbox_inches='tight')
    plt.close()

    # plot time
    index = range(len(p_size))
    plt.plot(index, time)
    plt.xticks(index, p_size)
    plt.grid(True)
    plt.xlabel('Problemgröße')
    plt.ylabel('Zeit in Minuten')
    if problem_name == 'ksp':
        plt.savefig('../sol/ksp/pics/teilmengen({0})_{1}_{2}_{3}_time.png'.format(k, start_size, end_size, step_size), bbox_inches='tight')
    else:
        plt.savefig('../sol/mst/pics/teilmengen({0})_{1}_{2}_{3}_{4}_time.png'.format(k, complete_graph, start_size, end_size, step_size), bbox_inches='tight')
    plt.close()

    # plot gap
    index = range(len(p_size))
    plt.bar(index, gap)
    plt.xticks(index, p_size)
    plt.gca().yaxis.grid(True)
    plt.xlabel('Problemgröße')
    plt.ylabel('Gap')
    if problem_name == 'ksp':
        plt.savefig('../sol/ksp/pics/teilmengen({0})_{1}_{2}_{3}_gap.png'.format(k, start_size, end_size, step_size), bbox_inches='tight')
    else:
        plt.savefig('../sol/mst/pics/teilmengen({0})_{1}_{2}_{3}_{4}_gap.png'.format(k, complete_graph, start_size, end_size, step_size), bbox_inches='tight')
    plt.close()

    # plot frequency
    index = range(len(p_size))
    plt.bar(index, freq)
    plt.xticks(index, p_size)
    plt.gca().yaxis.grid(True)
    plt.xlabel('Problemgröße')
    plt.ylabel('Relative Häufigkeit')
    if problem_name == 'ksp':
        plt.savefig('../sol/ksp/pics/teilmengen({0})_{1}_{2}_{3}_freq.png'.format(k, start_size, end_size, step_size), bbox_inches='tight')
    else:
        plt.savefig('../sol/mst/pics/teilmengen({0})_{1}_{2}_{3}_{4}_freq.png'.format(k, complete_graph, start_size, end_size, step_size), bbox_inches='tight')
    plt.close()

    # plot nos and time


def plot_comparison_progress(problem_name, number_of_problems, problem_info, auswahl=False):
    subsets_list = cb.read_block_list_special(problem_name, problem_info, 'progress')
    file_beginning = '../sol/{0}/{1}_{2}_'.format(problem_name, problem_info[0], problem_info[1])

    # read values
    sol = {}
    gap = {}

    for name in subsets_list:
        if not name.find('zufall') == -1:
            continue

        file = open(file_beginning + name + '.eva', 'r')
        loop_values = {}

        for i in range(14):
            file.readline()

        for i in range(number_of_problems):

            file.readline()
            line = file.readline()
            loop_values[i] = [float(x) for x in line.split('; ')]

        file.close()

        loop_numbers = [len(loop_values[i]) for i in range(number_of_problems)]
        max_loop = max(loop_numbers)

        for i in range(number_of_problems):
            for k in range(max_loop - loop_numbers[i]):
                loop_values[i].append(loop_values[i][-1])

            # print(len(loop_values[i]), loop_values[i])

        sol[name] = []
        for k in range(max_loop):
            val = sum([loop_values[i][k] for i in range(number_of_problems)]) / number_of_problems
            sol[name].append(val)

        gap[name] = []
        for k in range(max_loop):
            if k == max_loop - 1:
                gap[name].append(0)
            else:
                mini_gap = sum([(abs(loop_values[i][k + 1] - loop_values[i][k]) / abs(loop_values[i][k + 1])) for i in range(number_of_problems)]) / number_of_problems
                gap[name].append(mini_gap)

    '''plotting'''
    # set colors
    cm = plt.get_cmap('gist_rainbow')
    colors = [cm(1. * i / len(subsets_list)) for i in range(len(subsets_list))]

    # solution
    max_loop = 0
    fig = plt.figure()
    ax = plt.subplot(111)
    ax.set_prop_cycle('color', colors)
    for name in subsets_list:
        if not name.find('zufall') == -1:
            continue
        loops = len(sol[name])
        if loops > max_loop:
            max_loop = loops
        plt.plot(range(loops-1), sol[name][1:], '-o', label=name)
    plt.xticks(range(max_loop-1), range(2, max_loop+1))
    handles, labels = ax.get_legend_handles_labels()
    lgd = ax.legend(handles, labels, loc='center left', bbox_to_anchor=(1, 0.5))
    plt.xlabel('Anzahl Runden')
    plt.ylabel('Wert')
    plt.grid(True)
    if auswahl:
        fig.savefig('../sol/{0}/pics/{1}_{2}_progress_2_sol.png'.format(problem_name, problem_info[0], problem_info[1]), bbox_extra_artists=(lgd,), bbox_inches='tight')
    else:
        fig.savefig('../sol/{0}/pics/{1}_{2}_progress_sol.png'.format(problem_name, problem_info[0], problem_info[1]), bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.close()

    # gap
    max_loop = 0
    fig = plt.figure()
    ax = plt.subplot(111)
    ax.set_prop_cycle('color', colors)
    for name in subsets_list:
        if not name.find('zufall') == -1:
            continue
        loops = len(sol[name])
        if loops > max_loop:
            max_loop = loops
        plt.plot(range(loops-1), gap[name][1:], '-o', label=name)
    plt.xticks(range(max_loop-1), range(2, max_loop+1))
    handles, labels = ax.get_legend_handles_labels()
    lgd = ax.legend(handles, labels, loc='center left', bbox_to_anchor=(1, 0.5))
    plt.xlabel('Anzahl Runden')
    plt.ylabel('Relative Änderung')
    plt.grid(True)
    if auswahl:
        plt.savefig('../sol/{0}/pics/{1}_{2}_progress_2_gap.png'.format(problem_name, problem_info[0], problem_info[1]), bbox_extra_artists=(lgd,), bbox_inches='tight')
    else:
        plt.savefig('../sol/{0}/pics/{1}_{2}_progress_gap.png'.format(problem_name, problem_info[0], problem_info[1]), bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.close()


def plot_random_blocks(problem_name, number_of_problems, problem_info, block_size, start_loop=0):
    opt_val, aver = eva.read_optimal_solutions(problem_name, problem_info)

    file = open('../sol/{0}/{1}_{2}_zufall({3}).eva'.format(problem_name, problem_info[0], problem_info[1], block_size), 'r')

    for i in range(14):
        file.readline()

    fig = plt.figure(figsize=(15, 8))
    ax = fig.gca()
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    plt.xlabel('Anzahl Runden')
    plt.ylabel('Wert')

    p = {}
    for i in range(number_of_problems):
        file.readline()
        line = file.readline()
        loop_values = [float(x) for x in line.split('; ')]
        index = list(range(start_loop, start_loop + len(loop_values[start_loop:])))
        p[i] = plt.plot(index, loop_values[start_loop:], '-', label='Instanz ' + str(i + 1))
        plt.plot([start_loop + len(loop_values[start_loop:]) - 1], [opt_val[i]], marker='x', markersize=10, color=p[i][0].get_color())
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.grid(True)
    handles, labels = ax.get_legend_handles_labels()
    lgd = ax.legend(handles, labels, loc='center left', bbox_to_anchor=(1, 0.5))
    fig.savefig('../sol/{0}/pics/{1}_{2}_zufall({3})_{4}.png'.format(problem_name, problem_info[0], problem_info[1], block_size, start_loop), bbox_extra_artists=(lgd,), bbox_inches='tight')

    plt.close()


def plot_all_k_subs_comparison(p_size=None):
    if not p_size:
        p_size = [18, 20, 21]

    for n in p_size:
        b = [scipy.special.binom(n, i) for i in range(1, n + 1)]
        plt.plot(range(len(b)), b, '-o', label='{0} Elemente'.format(n))

    plt.xticks(range(max(p_size)), range(1, max(p_size) + 1))
    plt.xlabel('k-elementige Teilmengen')
    plt.ylabel('Anzahl Blöcke')
    plt.gca().yaxis.grid(True)
    plt.gca().get_yaxis().get_major_formatter().set_scientific(False)
    plt.legend()
    plt.savefig('../pics/k-el-subs_comparison_nos_{0}.png'.format(len(p_size)), bbox_inches='tight')
    plt.close()


# plot_time_sol('ksp', [10000, 50000], True)
# plot_time_sol('mst', [141, 9870], True)
# plot_time_sol('mst', [173, 9918], True)
#
# plot_nos_time('ksp', [10000, 50000], True)
# plot_nos_time('mst', [141, 9870], True)
# plot_nos_time('mst', [173, 9918], True)
#
# plot_nos_loops('ksp', [10000, 50000], True)
# plot_nos_loops('mst', [141, 9870], True)
# plot_nos_loops('mst', [173, 9918], True)
#
# plot_comparison('ksp', [10000, 50000])
# plot_comparison('mst', [141, 9870])
# plot_comparison('mst', [173, 9918])
#
# plot_comparison_sample('ksp', [10000, 50000], 3)
# plot_comparison_sample('ksp', [10000, 50000], 3)
# plot_comparison_sample('mst', [141, 9870], 2)
# plot_comparison_sample('mst', [173, 9918], 2)
#
# plot_comparison_progress('ksp', 10, [10000, 50000], False)
# plot_comparison_progress('mst', 10, [141, 9870], False)
# plot_comparison_progress('mst', 10, [173, 9918], True)
#
# plot_random_blocks('ksp', 10, [10000, 50000], 100, 0)
# plot_random_blocks('mst', 10, [141, 9870], 100, 0)
# plot_random_blocks('mst', 10, [173, 9918], 100, 0)
#
# plot_random_blocks('ksp', 10, [10000, 50000], 1000, 0)
# plot_random_blocks('mst', 10, [141, 9870], 1000, 0)
# plot_random_blocks('mst', 10, [173, 9918], 1000, 0)
#
# plot_k_el_subs('ksp', 2, 100, 1000, 100)
# plot_k_el_subs('mst', 2, 16, 44, 3, complete_graph=False)
# plot_k_el_subs('mst', 2, 15, 36, 2, complete_graph=True)
#
# plot_k_el_subs('ksp', 3, 30, 91, 10)
# plot_k_el_subs('mst', 3, 9, 18, 1, complete_graph=False)
# plot_k_el_subs('mst', 3, 7, 15, 1, complete_graph=True)
#
# plot_k_el_subs('ksp', 4, 25, 46, 5)
# plot_k_el_subs('mst', 4, 9, 13, 1, complete_graph=False)
# plot_k_el_subs('mst', 4, 7, 11, 1, complete_graph=True)
#
# plot_all_k_subs('mst', [8, 18])
# plot_all_k_subs('mst', [7, 21])
# plot_all_k_subs('ksp', [20, 100])
#
# plot_all_k_subs_comparison([18,20,21])
# plot_all_k_subs_comparison([18,20,21,30])
