import sys
sys.path.insert(0, '../src/')

import create_blocks as cb
import evaluation as eva

number_of_problems = 10
number_of_elements = 10000
capacity = 50000
problem_info = [number_of_elements, capacity]

blocks, block_info = cb.ueberlapp(number_of_elements, 2)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.ueberlapp_rev(number_of_elements, 2)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)

blocks, block_info = cb.ueberlapp(number_of_elements, 3)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.ueberlapp_rev(number_of_elements, 3)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)

blocks, block_info = cb.ueberlapp(number_of_elements, 4)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.ueberlapp_rev(number_of_elements, 4)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)

blocks, block_info = cb.ueberlapp(number_of_elements, 100)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.ueberlapp_rev(number_of_elements, 100)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)

##########################################################################################

blocks, block_info = cb.ueberlapp_mod(number_of_elements, 2)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.ueberlapp_mod_rev(number_of_elements, 2)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)

blocks, block_info = cb.ueberlapp_mod(number_of_elements, 3)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.ueberlapp_mod_rev(number_of_elements, 3)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)

blocks, block_info = cb.ueberlapp_mod(number_of_elements, 4)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.ueberlapp_mod_rev(number_of_elements, 4)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)

blocks, block_info = cb.ueberlapp_mod(number_of_elements, 100)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.ueberlapp_mod_rev(number_of_elements, 100)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)

##########################################################################################

blocks, block_info = cb.disjunkt(number_of_elements, 2)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.disjunkt_rev(number_of_elements, 2)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)

blocks, block_info = cb.disjunkt(number_of_elements, 3)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.disjunkt_rev(number_of_elements, 3)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)

blocks, block_info = cb.disjunkt(number_of_elements, 4)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.disjunkt_rev(number_of_elements, 4)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)

blocks, block_info = cb.disjunkt(number_of_elements, 100)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)
blocks, block_info = cb.disjunkt_rev(number_of_elements, 100)
eva.calculating_one_block_set('ksp', number_of_problems, problem_info, blocks, block_info)
