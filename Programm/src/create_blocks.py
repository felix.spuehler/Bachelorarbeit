import itertools as it
import os.path as os
import random


def teilmengen(number_of_elements, k):
    elements = list(range(1, number_of_elements + 1))

    if k == 1:
        blocks = [[i] for i in elements]
    else:
        blocks = list(it.combinations(elements, k))

    block_info = ['teilmengen({0})'.format(k), len(blocks)]
    return blocks, block_info


def teilmengen_rev(number_of_elements, k):
    blocks, block_info = teilmengen(number_of_elements, k)
    blocks = list(reversed(blocks))
    block_info[0] = 'teilmengen_rev({0})'.format(k)

    return blocks, block_info


########################################################################################################################
def ueberlapp(number_of_elements, ld):
    # e.g.2: 1: 1 -> n/2, 2: n/2 -> n, 3: n/4 -> 3n/4
    blocks = []
    for i in range(ld):
        s = int(i * number_of_elements / ld) + 1
        t = int((i + 1) * number_of_elements / ld)
        blocks.append([s, t])

        if t + int(0.5 * number_of_elements / ld) > number_of_elements:
            break

        s += int(0.5 * number_of_elements / ld)
        t += int(0.5 * number_of_elements / ld)
        blocks.append([s, t])

    block_info = ['ueberlapp({0})'.format(ld), len(blocks)]

    return blocks, block_info


def ueberlapp_rev(number_of_elements, length_divisor):
    blocks, block_info = ueberlapp(number_of_elements, length_divisor)
    blocks = list(reversed(blocks))
    block_info[0] = 'ueberlapp_rev({0})'.format(length_divisor)

    return blocks, block_info


########################################################################################################################
def ueberlapp_mod(number_of_elements, ld):
    # e.g.2: 1: 1 -> n/2, 2: n/2 -> n, 3: n/4 -> 3n/4
    blocks = []
    for i in range(ld):
        s = int(i * number_of_elements / ld) + 1
        t = int((i + 1) * number_of_elements / ld)
        blocks.append([s, t])

        s += int(0.5 * number_of_elements / ld)
        t += int(0.5 * number_of_elements / ld)
        blocks.append([s, t])

    block_info = ['ueberlapp_mod({0})'.format(ld), len(blocks)]

    return blocks, block_info


def ueberlapp_mod_rev(number_of_elements, length_divisor):
    blocks, block_info = ueberlapp_mod(number_of_elements, length_divisor)
    blocks = list(reversed(blocks))
    block_info[0] = 'ueberlapp_mod_rev({0})'.format(length_divisor)

    return blocks, block_info


########################################################################################################################
def disjunkt(number_of_elements, ld):
    # disjoint blocks of same size; 1: 1 -> n/m, 2: n/m -> 2n/m, ...

    blocks = []
    for i in range(ld):
        s = int(i*number_of_elements/ld) + 1
        t = int((i+1)*number_of_elements/ld)
        blocks.append([s, t])

    block_info = ['disjunkt({0})'.format(ld), len(blocks)]
    return blocks, block_info


def disjunkt_rev(number_of_elements, length_divisor):
    blocks, block_info = disjunkt(number_of_elements, length_divisor)
    blocks = list(reversed(blocks))
    block_info[0] = 'disjunkt_rev({0})'.format(length_divisor)

    return blocks, block_info


########################################################################################################################
def zufall(elements, size):
    # random blocks of size ' + str(size) + '; not necessarily disjoint

    blocks = []
    if size >= len(elements):
        raise Exception('size has to be smaller!')
    used = {}
    all_used = False
    for e in elements:
        used[e] = False

    while not all_used:
        s = []
        while len(s) < size:
            new = elements[random.randint(0, len(elements) - 1)]
            if new not in s:
                s.append(new)
                used[new] = True
        blocks.append(s)

        all_used = True
        for key, val in used.items():
            if not val:
                all_used = False

    block_info = ['zufall({0})'.format(size), len(blocks)]
    return blocks, block_info


########################################################################################################################
########################################################################################################################
def add_to_block_list(problem_name, problem_info, block_info):
    filename = '../sol/{0}/{1}_{2}_list.eva'.format(problem_name, problem_info[0], problem_info[1])

    if not os.isfile(filename):
        file = open(filename, 'w')
        file.write(block_info[0])
        file.close()
        print('new file')
        print('and list.eva - file')
    else:
        file = open(filename, 'r+')
        bs_list = []
        for line in file.readlines():
            bs_list.append(line[:-1])

        if not block_info[0] in bs_list:
            file.write('\n{0}'.format(block_info[0]))
            print('new file')
        else:
            print('file overwritten')
        file.close()


def read_block_list(problem_name, problem_info):
    file = open('../sol/{0}/{1}_{2}_list.eva'.format(problem_name, problem_info[0], problem_info[1]), 'r')
    bs_list = file.read().splitlines()
    file.close()

    return bs_list


def read_block_list_special(problem_name, problem_info, plot_name):
    name = '../sol/{0}/{1}_{2}_list_{3}.eva'.format(problem_name, problem_info[0], problem_info[1], plot_name)
    if not os.exists(name):
        raise Exception('no such a list file')

    file = open(name, 'r')
    bs_list = file.read().splitlines()
    file.close()

    return bs_list
