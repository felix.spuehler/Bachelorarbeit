from datetime import datetime as dt

from random import randint
import networkx as nx
from time import *
from gurobipy import *


class Graph(object):
    def __init__(self):
        self.filename = ''

        self.edge_index = {}
        self.edge_weight = {}
        self.nodes = []
        self.number_of_nodes = 0
        self.edges = []
        self.number_of_edges = 0
        self.arcs = []

        self.cur_optimum = 0
        self.cur_time = 0
        self.cur_status = ''
        self.cur_x = {}

    # initialize data
    def initialize_data(self, number_of_nodes, number_of_edges, edges):
        # clear data
        self.edge_index = {}
        self.edge_weight = {}
        self.edges = []
        self.arcs = []

        for edge in edges:
            if edge[1] <= edge[2]:
                self.edges.append((edge[1], edge[2]))
                self.edge_index[edge[0]] = (edge[1], edge[2])
                self.edge_weight[(edge[1], edge[2])] = edge[3]
            else:
                self.edges.append((edge[2], edge[1]))
                self.edge_index[edge[0]] = (edge[2], edge[1])
                self.edge_weight[(edge[2], edge[1])] = edge[3]

        self.nodes = range(1, number_of_nodes + 1)
        self.number_of_nodes = number_of_nodes
        self.number_of_edges = number_of_edges

        for u, v in self.edges:
            self.arcs.append((u, v))
            self.arcs.append((v, u))

        self.start_allocation()

    # generate start allocation
    def start_allocation(self):
        g = nx.Graph()
        g.add_edges_from(self.edges)
        start_tree = nx.minimum_spanning_tree(g)
        self.cur_x = {(u, v): (1 if start_tree.has_edge(u, v) else 0) for (u, v) in self.edges}
        self.cur_optimum = sum([self.cur_x[e] * self.edge_weight[e] for e in self.edges])

    # generate list of elements for a specific block
    def block_elements(self, ids):
        start = ids[0]
        end = ids[1]

        lst = []
        if ids[1] <= self.number_of_edges:
            for i in range(start, end + 1):
                lst.append(self.edge_index[i])
        else:
            for i in range(start, self.number_of_edges + 1):
                lst.append(self.edge_index[i])
            for i in range(1, end % self.number_of_edges + 1):
                lst.append(self.edge_index[i])

        return lst

    # solve complete problem with gurobi
    def solve_complete(self):
        try:
            # create model
            prob = Model('mst')
            prob.Params.OutputFlag = 0
            # create variables
            x = prob.addVars(self.edges, vtype=GRB.BINARY)
            f = prob.addVars(self.arcs, vtype=GRB.CONTINUOUS)
            # set objective
            expr = quicksum([self.edge_weight[e] * x[e] for e in x])
            prob.setObjective(expr, GRB.MINIMIZE)
            # add constraints
            for c, n in enumerate(self.nodes):
                expr = quicksum([f[(i, j)] for (i, j) in self.arcs if i == n]) - sum([f[(j, i)] for (j, i) in self.arcs if i == n])
                if c == 0:
                    prob.addConstr(expr, GRB.EQUAL, self.number_of_nodes - 1, name='flow at {0}'.format(c))
                else:
                    prob.addConstr(expr, GRB.EQUAL, - 1, name='flow at {0}'.format(c))

            for (i, j) in self.edges:
                prob.addConstr(f[(i, j)], GRB.LESS_EQUAL, (self.number_of_nodes - 1) * x[(i, j)], name='first arc restriction for {0}'.format((i, j)))
                prob.addConstr(f[(j, i)], GRB.LESS_EQUAL, (self.number_of_nodes - 1) * x[(i, j)], name='second arc restriction for {0}'.format((i, j)))

            prob.addConstr(sum([x[e] for e in x]), GRB.EQUAL, self.number_of_nodes - 1, 'tree restriction')

            # set start solution
            for e in self.edges:
                x[e].start = self.cur_x[e]

            # solve
            t1 = clock()
            prob.optimize()
            t2 = clock()
            t = t2 - t1

            for e in self.edges:
                self.cur_x[e] = int(x[e].x)
            self.cur_optimum = prob.objVal
            self.cur_time = t
            self.cur_status = prob.status

        except GurobiError as e:
            print('Error {0}'.format(e))

        return self.cur_optimum

    # solve algorithm with block set
    def algorithm(self, blocks, block_info):
        try:
            '''Modelling'''
            # create model
            prob = Model('mst')
            prob.Params.OutputFlag = 0
            # create variables
            x = prob.addVars(self.edges, vtype=GRB.BINARY)
            f = prob.addVars(self.arcs, vtype=GRB.CONTINUOUS)
            # set objective
            expr = quicksum([self.edge_weight[e] * x[e] for e in x])
            prob.setObjective(expr, GRB.MINIMIZE)
            # add constraints
            for c, n in enumerate(self.nodes):
                expr = quicksum([f[(i, j)] for (i, j) in self.arcs if i == n]) - sum([f[(j, i)] for (j, i) in self.arcs if i == n])
                if c == 0:
                    prob.addConstr(expr, GRB.EQUAL, self.number_of_nodes - 1, name='flow at {0}'.format(c))
                else:
                    prob.addConstr(expr, GRB.EQUAL, - 1, name='flow at {0}'.format(c))

            for (i, j) in self.edges:
                prob.addConstr(f[(i, j)], GRB.LESS_EQUAL, (self.number_of_nodes - 1) * x[(i, j)], name='first arc restriction for {0}'.format((i, j)))
                prob.addConstr(f[(j, i)], GRB.LESS_EQUAL, (self.number_of_nodes - 1) * x[(i, j)], name='second arc restriction for {0}'.format((i, j)))

            prob.addConstr(sum([x[e] for e in x]), GRB.EQUAL, self.number_of_nodes - 1, 'tree restriction')

            # set start solution
            for i, e in self.edge_index.items():
                x[e].lb = self.cur_x[e]
                x[e].ub = self.cur_x[e]
            prob.update()
            prob.optimize()

            '''Algorithm'''
            counter = 1
            old_val = self.cur_optimum
            loop_val = [prob.objVal]

            time_start = dt.now()
            while True:
                for block in blocks:
                    # generate block list
                    if block_info[0].find('teilmengen') == -1:
                        lst = self.block_elements(block)
                    else:
                        lst = [self.edge_index[i] for i in block]

                    # set boundaries, e.g. free variables in lst, fix variables in lst_c
                    if block_info[0].find('teilmengen') == -1:
                        for i, e in self.edge_index.items():
                            if block[0] <= i <= block[1]:
                                x[e].lb = 0
                                x[e].ub = 1
                            elif block[1] >= self.number_of_edges and i <= (block[1] % self.number_of_edges):
                                x[e].lb = 0
                                x[e].ub = 1
                            else:
                                x[e].lb = int(x[e].x)
                                x[e].ub = int(x[e].x)


                    else:
                        for i, e in self.edge_index.items():
                            if i in block:
                                x[e].lb = 0
                                x[e].ub = 1
                            else:
                                x[e].lb = int(x[e].x)
                                x[e].ub = int(x[e].x)

                    # update
                    prob.update()

                    # solve
                    prob.optimize()
                    for e in lst:
                        self.cur_x[e] = int(x[e].x)

                    self.cur_optimum = prob.objVal
                    self.cur_status = prob.status

                if old_val == prob.objVal:
                    break

                loop_val.append(prob.objVal)
                old_val = prob.objVal
                counter += 1

            time_end = dt.now()
            time_algo = (time_end - time_start).total_seconds()

            print()
            return counter, time_algo, loop_val

        except GurobiError as e:
            print('Error {0}'.format(e))

    # solve algorithm with random block set
    def algorithm_random(self, block_size):
        try:
            '''Modelling'''
            # create model
            prob = Model('mst')
            prob.Params.OutputFlag = 0
            # create variables
            x = prob.addVars(self.edges, vtype=GRB.BINARY)
            f = prob.addVars(self.arcs, vtype=GRB.CONTINUOUS)
            # set objective
            expr = quicksum([self.edge_weight[e] * x[e] for e in x])
            prob.setObjective(expr, GRB.MINIMIZE)
            # add constraints
            for c, n in enumerate(self.nodes):
                expr = quicksum([f[(i, j)] for (i, j) in self.arcs if i == n]) - sum([f[(j, i)] for (j, i) in self.arcs if i == n])
                if c == 0:
                    prob.addConstr(expr, GRB.EQUAL, self.number_of_nodes - 1, name='flow at {0}'.format(c))
                else:
                    prob.addConstr(expr, GRB.EQUAL, - 1, name='flow at {0}'.format(c))

            for (i, j) in self.edges:
                prob.addConstr(f[(i, j)], GRB.LESS_EQUAL, (self.number_of_nodes - 1) * x[(i, j)], name='first arc restriction for {0}'.format((i, j)))
                prob.addConstr(f[(j, i)], GRB.LESS_EQUAL, (self.number_of_nodes - 1) * x[(i, j)], name='second arc restriction for {0}'.format((i, j)))

            prob.addConstr(sum([x[e] for e in x]), GRB.EQUAL, self.number_of_nodes - 1, 'tree restriction')

            # set start solution
            for i, e in self.edge_index.items():
                x[e].lb = self.cur_x[e]
                x[e].ub = self.cur_x[e]
            prob.update()
            prob.optimize()

            '''Algorithm'''
            counter = 1
            old_val = self.cur_optimum
            loop_val = [prob.objVal]

            dict_used = {i: 0 for i in self.edge_index.keys()}
            check_used = False

            time_start = dt.now()
            while True:
                if counter%100 == 1:
                    print(counter)
                # generate random block list
                lst = []
                for i in range(block_size):
                    ri = randint(1, self.number_of_edges)
                    while ri in lst:
                        ri = randint(1, self.number_of_edges)

                    lst.append(self.edge_index[ri])
                    dict_used[ri] += 1

                # set boundaries, e.g. free variables in lst, fix variables in lst_c
                for i, e in self.edge_index.items():
                    if e in lst:
                        x[e].lb = 0
                        x[e].ub = 1
                    else:
                        x[e].lb = int(x[e].x)
                        x[e].ub = int(x[e].x)

                # update
                prob.update()

                # solve
                prob.optimize()

                for e in lst:
                    self.cur_x[e] = int(x[e].x)

                self.cur_optimum = prob.objVal
                self.cur_status = prob.status

                # check if all elements are used
                if not check_used:
                    c = True
                    for t in dict_used.values():
                        if t == 0:
                            c = False
                            break
                    if c:
                        check_used = True

                if old_val == prob.objVal and check_used:
                    break

                loop_val.append(prob.objVal)
                old_val = prob.objVal
                counter += 1

            time_end = dt.now()
            time_algo = (time_end - time_start).total_seconds()

            return counter, time_algo, loop_val, dict_used

        except GurobiError as e:
            print('Error {0}'.format(e))
