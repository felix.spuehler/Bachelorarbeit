import os.path as os
import random
import graph as gr


# create edges for a random (karg) graph
def create_random_graph_edges(number_of_nodes, number_of_edges):
    edges = []
    elements = []

    used_nodes = [1]
    unused_nodes = list(range(2, number_of_nodes+1))

    edge_id = 1
    left_id = 1
    right_id = unused_nodes[random.randint(0, len(unused_nodes)-1)]  # random unused node
    used_nodes.append(right_id)
    unused_nodes.remove(right_id)
    edges.append([edge_id, left_id, right_id, 0])
    elements.append((left_id, right_id))

    while edge_id < number_of_nodes-1:
        edge_id += 1
        left_id = used_nodes[random.randint(0, len(used_nodes)-1)]  # random used node
        right_id = unused_nodes[random.randint(0, len(unused_nodes)-1)]  # random unused node
        used_nodes.append(right_id)
        unused_nodes.remove(right_id)
        edges.append([edge_id, left_id, right_id, 0])
        elements.append((left_id, right_id))

    while edge_id < number_of_edges:
        edge_id += 1
        left_id = used_nodes[random.randint(0, number_of_nodes - 1)]  # random used node
        right_id = used_nodes[random.randint(0, number_of_nodes - 1)]  # random used node
        while right_id == left_id:
            right_id = used_nodes[random.randint(0, len(used_nodes) - 1)]
        if (left_id, right_id) in elements or (right_id, left_id) in elements:
            edge_id -= 1
            continue
        edges.append([edge_id, left_id, right_id, 0])
        elements.append((left_id, right_id))

    return elements, edges


# create edges for a complete graph
def create_complete_graph_edges(number_of_nodes):
    elements = []
    edges = []
    edge_id = 0

    for left_id in range(1, number_of_nodes+1):
        for right_id in range(left_id+1, number_of_nodes+1):
            edge_id += 1
            edges.append([edge_id, left_id, right_id, 0])
            elements.append((left_id, right_id))
    return elements, edges


# generate random data sets for given edges (weight for each edge)
def create_random_data_set(edges, number_of_edges):
    for edge in edges:
        edge[3] = random.randint(1, 2*number_of_edges)  # random weight

    return edges


# generate random instances
def generate_random_problem(number_of_nodes, complete):
    tri_numb = int(number_of_nodes * (number_of_nodes - 1) / 2)

    if complete:
        number_of_edges = tri_numb
        elements, edges = create_complete_graph_edges(number_of_nodes)
    else:
        number_of_edges = int(2/3 * tri_numb)
        elements, edges = create_random_graph_edges(number_of_nodes, number_of_edges)
    edges = create_random_data_set(edges, number_of_edges)

    problem = gr.Graph()
    problem.initialize_data(number_of_nodes, number_of_edges, edges)

    return problem


# write file of instances
def write_problem_file(number_of_problems, number_of_nodes, complete):
    tri_numb = int(number_of_nodes * (number_of_nodes - 1) / 2)

    if complete:
        number_of_edges = tri_numb
        print('Complete graph with (only) {0} edges.'.format(number_of_edges))
        elements, edges = create_complete_graph_edges(number_of_nodes)
    else:
        number_of_edges = int(2/3 * tri_numb)
        print('Graph with {0} edges.'.format(number_of_edges))
        elements, edges = create_random_graph_edges(number_of_nodes, number_of_edges)

    # check if data set already exists
    name = '../basis/mst/{0}_{1}_{2}_info.dat'.format(number_of_nodes, number_of_edges, complete)
    if os.exists(name):
        raise Exception('files already exists')

    # write info file
    file = open(name, 'w')
    file.write('# number of problems; {0}\n'.format(number_of_problems))
    file.write('# number of nodes; {0}\n'.format(number_of_nodes))
    file.write('# number of edges; {0}\n'.format(number_of_edges))
    file.write('# complete graph; {0}\n\n'.format(complete))
    file.write('# list of elements in extra file')
    file.close()

    # write edges file
    name = '../basis/mst/{0}_{1}_edges.dat'.format(number_of_nodes, number_of_edges)
    file = open(name, 'w')
    file.write('# left-stop; right-stop')
    for e in edges:
        file.write('\n')
        file.write('{0}; {1}'.format(e[1], e[2]))
    file.close()

    # write data files
    for i in range(number_of_problems):
        name = '../basis/mst/{0}_{1}_{2}.dat'.format(number_of_nodes, number_of_edges, i)
        edges = create_random_data_set(edges, number_of_edges)

        file = open(name, 'w')
        file.write('# event-id; left-stop; right-stop; weight')
        for e in edges:
            file.write('\n')
            file.write('{0}; {1}; {2}; {3}'.format(e[0], e[1], e[2], e[3]))
        file.close()


# read the file of instances
def read_problem_file(number_of_nodes, number_of_edges, number):
    # read problem file
    file = open('../basis/mst/{0}_{1}_{2}.dat'.format(number_of_nodes, number_of_edges, number), 'r')
    file.readline()

    edges = []
    for line in file.readlines():
        l_index, l_left, l_right, l_weight = line.split('; ')
        edge = [int(l_index), int(l_left), int(l_right), int(l_weight)]
        edges.append(edge)
    problem = gr.Graph()
    problem.initialize_data(number_of_nodes, number_of_edges, edges)

    return problem

# read the file with list of edges
def list_of_elements(number_of_nodes, number_of_edges):
    file = open('../basis/mst/{0}_{1}_edges.dat'.format(number_of_nodes, number_of_edges), 'r')
    file.readline()

    elements = []
    for line in file.readlines():
        left_stop, right_stop = line.split('; ')
        elements.append((int(left_stop), int(right_stop)))

    return elements
    # evtl wird auch nur eine Liste von den Ids gebraucht. Das geht dann einfacher...
