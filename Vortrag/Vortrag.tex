\documentclass[11pt]{beamer}
\usetheme[hideothersubsections]{Goettingen}
\usecolortheme{orchid}

\usepackage[utf8]{inputenc}
\usepackage[german]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{framed}
\usepackage[ruled]{algorithm2e}
\usepackage{float}
\usepackage{caption}
\usepackage{subfig}

\newcommand{\RR}{\mathbb{R}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\NN}{\mathbb{N}}


\newtheorem{defi}{Definition}
\newtheorem{satz}{Satz}
%%newtheorem{lemma}[defi]{Lemma}
\newtheorem{folg}{Folgerung}
%\newtheorem{bsp}[defi]{Beispiel}
\newtheorem{bem}{Bemerkung}
\newtheorem{nota}{Notation}
%\newtheorem{frage}[defi]{Fragestellung}
%\newtheorem{vermutung}[defi]{Vermutung}

\newcommand*{\ksp}{../Programm/sol/ksp/pics/}
\newcommand*{\mst}{../Programm/sol/mst/pics/}
\newcommand{\pics}{../Programm/pics/}
\newcommand{\Bilder}{../LaTeX/Bilder/}

\resetcounteronoverlays{algocf}
\SetAlgorithmName{Algorithmus}{heuristic}{Verzeichnis der Algorithmen}
\beamertemplatenavigationsymbolsempty
\setbeamertemplate{footline}[frame number]

\author{Felix Spühler}
\title{Blockkoordinaten-Abstiegsverfahren für kombinatorische Probleme}
%\setbeamercovered{transparent} 
%\setbeamertemplate{navigation symbols}{} 
%\logo{} 
\institute{Institut für Numerische und Angewandte Mathematik} 
\date{17.04.2018} 
%\subject{test} 


\begin{document}
\begin{frame}
\titlepage
\end{frame}
\begin{frame}
\tableofcontents
\end{frame}


\section{Spannbäume und Rucksäcke}
{\frame{   \frametitle{Überblick}   \tableofcontents[currentsection]}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Problem des minimalen Spannbaums}
\begin{frame}{Formulierung}
\small
\begin{framed}
\textbf{$(\mathcal{MST})$ Problem des minimalen Spannbaums:} \\
\textbf{Gegeben} sei ein Graph $G=(V,E)$ mit einer Gewichtsfunktion $c: E \rightarrow \RR$.\\ 
\textbf{Gesucht} ist ein minimaler Spannbaum $T^*$ von $G$.
\end{framed}
\pause
\normalsize
\begin{alignat*}{3}
\mbox{min } &&\sum_{e \in E} c_e x_e\\
\mbox{s.d. } &&\sum_{e \in E} x_e &= |V| - 1\\
&&\sum_{e=\{u,v\} \in E: i \in S, j \in S} x_e &\leq |S| - 1 \quad \forall S \subseteq V\\
&&x_e &\in \{0,1\} \quad \forall e \in E
\end{alignat*}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Formulierung als Netzwerk-Problem}
\small

\begin{alignat*}{3}
\mbox{min } &&\sum_{e \in E} c_e x_e\\
\mbox{s.d. } &&\sum_{(u,v) \in A} f_{uv} - \sum_{(v,u) \in A} f_{vu} &=
\begin{cases}
|V| - 1 &\text{falls } u = q\\
-1 &\forall u \in V \setminus\{q\}
\end{cases}\\
&&f_{uv} &\leq (|V| - 1) \cdot x_e \quad \forall e= \{u,v\} \in E\\ 
&&f_{vu} &\leq (|V| - 1) \cdot x_e \quad \forall e= \{u,v\} \in E\\
&&\sum_{e \in E} x_e &= |V| - 1\\
&&f_{uv} &\geq 0 \quad \forall (u,v) \in A\\
&&x_e &\in \{0,1\} \quad \forall e \in E
\end{alignat*}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Beispiel}
\begin{minipage}{0.45\linewidth}
\centering
\includegraphics{\Bilder graph_network_1.pdf}
\end{minipage}
\pause
\begin{minipage}{0.45\linewidth}
\centering
\includegraphics{\Bilder graph_network_2.pdf}
\end{minipage}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Rucksack-Problem}
\begin{frame}{Formulierung}
\small
\begin{framed}
\textbf{$(\mathcal{KSP})$ Rucksack-Problem:} \\
\textbf{Gegeben} seien n Objekte und eine Kapazität $K$. Dazu wird jedem Objekt ein Gewicht $w_i$ und ein Wert $c_i$ für alle $ i = 1, \ldots, n$ zugeordnet.\\ 
\textbf{Gesucht} ist eine Auswahl an Objekten, sodass die Summe der Werte dieser Objekte maximal ist und das Gewicht kleiner oder gleich der Kapazität des Rucksacks ist.
\end{framed}
\normalsize
\pause
\begin{alignat*}{3}
\mbox{max } &&\sum_{i = 1}^n c_i x_i\\
\mbox{s.d. } &&\sum_{i = 1}^n w_i x_i &\leq K\\
&&x_i &\in \{0,1\} \quad \forall i = 1, \ldots n
\end{alignat*}
\end{frame}


\section{Blockkoordinaten-Abstiegsverfahren}
{\frame{   \frametitle{Überblick}   \tableofcontents[currentsection]}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Algorithmus}
\begin{frame}{Vorbereitung}
\begin{alignat*}{3}
(\mathcal{P}) \qquad \mbox{min} \qquad &&f(x_1, x_2, \ldots , x_n)\\
\mbox{s.d.} \qquad &&(x_1, x_2\ldots, x_n) &\in P\\
&&x_i &\in \{0,1\} \quad \forall i \in \{0, 1, \ldots n\}
\end{alignat*}
mit $f\,:\, \RR^n \rightarrow \RR$ und $P$ die Menge, die durch die Nebenbedingungen festgelegt ist.
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Vorbereitung}
\framesubtitle{Blockformulierung}
\begin{enumerate}[<+->]
\item $I \subseteq \{1, 2, \ldots, n\}$ beliebige Teilmenge
\item $I^C := \{1, 2, \ldots, n\} \setminus I$
\item Entscheidungsvariablen aufteilen:\\
$x_I \cup x_{I^C}$ mit $x_I \in \{0,1\}^{|I|}$ und $x_{I^c} \in \{0,1\}^{n - |I|}$
\item Setze $y \in \{0,1\}^{n - |I|}$ fest.
\end{enumerate}

\visible<5>{
\begin{alignat*}{3}
(\mathcal{P}_I) (y) \qquad \mbox{min} \qquad &&f(x_I, y)\\
\mbox{s.d.} \qquad &&(x_I, y) &\in P\\
&&x_I &\in \{0,1\}^{|I|}
\end{alignat*}
}
\end{frame}

\begin{frame}{Blockformulierung}
\framesubtitle{Formulierung mit Blöcken und Startlösung}
\begin{enumerate}[<+->]
\item Wähle $k$ Blöcke: $I_1, I_2, \ldots, I_k \in Pot(\{1, 2, \ldots, n\})$
\item Sei $\mathcal{I} := \{I_1, I_2, \ldots, I_k\}$ mit ${I_1 \cup I_2 \ldots \cup I_k = \{1, 2, \ldots, n\}}$.
\item Für Block $I_j$ und Startlösung $\xi = \{0,1\}^n$ definiere
\begin{itemize}
\item $\hat{x}_j := x_{I_j} = (x_i: \, i \in I_J)$, \quad $\bar{x}_j := x_{I_j^C} = (x_i: \, i \notin I_J)$
\item $\hat{\xi} := (\xi_i: \, i \in I_J)$, \quad $\bar{\xi} := (\xi_i: \, i \notin I_J)$
\end{itemize}
\item Definiere $\mathcal{P}_j (\xi) := \mathcal{P}_{I_j} (\bar{\xi})$.
\item[] \begin{minipage}{0.9\linewidth}
\begin{alignat*}{3}
(\mathcal{P}_j) (\xi) \qquad \mbox{min} \qquad &&f(x,\, \bar{\xi})\\
\mbox{s.d.} \qquad &&(x,\, \bar{\xi}) &\in P\\
&&x_i &\in \{0,1\} \quad \forall i \in I_j
\end{alignat*}
\end{minipage}

\item $x_j^* \in \{0,1\}^{|I_j|}$ optimal für $(\mathcal{P}_j) (\xi)$. \\
Dann ist $x := (x_j^*, \bar{\xi}) \in \{0,1\}^n$ Lösung für $(\mathcal{P})$.
\end{enumerate}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Algorithmus}
\begin{algorithm}[H]
\DontPrintSemicolon
%\KwIn{Test}
%\KwOut{Test2}
\caption{Blockkoordinaten"=Abstiegsverfahren}
Rate eine Startlösung $\xi$.\;
Wähle Blöcke $I_1, I_2, \ldots, I_k \subseteq \{1, \ldots, n\}$\;
\quad mit $I_1 \cup I_2 \ldots \cup I_k = \{1, \ldots, n\}$\;
\pause
\Repeat{$f(x^{(0)}) = f(\xi)$}{
	Setze $x^{(0)} = \xi$.\;
	\For{$j=1, \ldots, k$}{
		Löse $(\mathcal{P}_j)(x^{(j-1)})$\;
		Sei $x^*$ optimal für $(\mathcal{P}_j)(x^{(j-1)})$.\;
		Setze $x^{(j)} = (x^*, \bar{x}_{(j-1)})$.\;
		}
		Setze $\xi = x^{(k)}$.
		
	}
\end{algorithm}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Algorithmus}
\begin{lemma}
Betrachte das Blockkoordinaten"=Abstiegsverfahren mit einer beliebigen Blockeinteilung $\mathcal{I} = \{I_1, I_2, \ldots, I_k\}$.\\
Dann gilt, dass sich der aktuelle Wert von $x^{(j)}$ durch $(\mathcal{P}_j)$ für jeden Block $I_j \in \mathcal{I}$ entweder verbessert oder zumindest gleich bleibt.
\end{lemma}

\pause
\begin{bem}
Das Verfahren ist nur eine Heuristik!
\end{bem}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Formulierungen mit Blöcken}
\begin{frame}{Allgemein}
Weitere Formulierung für das Block-Optimierungsproblem:

\begin{alignat*}{4}
(\mathcal{P}_j') (\xi) \qquad \mbox{min} \qquad &&f(x_1, x_2, \ldots, x_n)\\
\mbox{s.d.} \qquad &&(x_1, x_2, \ldots, x_n) &\in P\\
&&x_i &= \xi_i \quad &&\forall i \notin I_j\\
&&x_i &\in \{0,1\} \quad &&\forall i \in I_j
\end{alignat*}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Problem des minimalen Spannbaums}
\begin{frame}{Erste Formulierung: $(\mathcal{MST}_j')(\xi)$}
\small
\begin{alignat*}{3}
&\mbox{min } \quad &\sum_{e \in E} c_e x_e\\
&\mbox{s.d. } &\sum_{(u,v) \in A} f_{uv} - \sum_{(v,u) \in A} f_{vu} &=
\begin{cases}
|V| - 1 &\text{falls } u = q\\
-1 &\forall u \in V\setminus\{q\}
\end{cases}\\
&&f_{uv} &\leq (|V| - 1) \cdot x_e \quad \forall e= \{u,v\} \in E\\ 
&&f_{vu} &\leq (|V| - 1) \cdot x_e \quad \forall e= \{u,v\} \in E\\
&&\sum_{e \in E} x_e &= |V| - 1\\
&&f_{uv} &\geq 0 \quad \forall (u,v) \in A\\
&&x_e &\in \{0,1\} \quad \forall e \in E\\
&&x_e &=\xi_e \quad \forall e \notin I_j
\end{alignat*}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Aussagen über die Entscheidungsvariablen}
Somit gilt für die Kanten $e \in E$ mit $e \notin I_j$:
\pause
\begin{itemize}[<+->]
\item Ist $\xi_e = x_e = 1$:\\
Kante $e$ ist und bleibt Teil des Spannbaums.
\item Ist $\xi_e = x_e = 0$:\\
Kante $e$ ist und bleibt \textit{nicht} Teil des Spannbaums.
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Aussagen über die Flussvariablen}
\underline{Wir haben:} Startlösung $\xi$, Block $I_j$, Kante $e = \{u,v\}$, Entscheidungsvariable $x_e$ und die Flussvariablen $f_{uv}$ und $f_{vu}$
\pause
\small
\begin{enumerate}[<+->]
\item Die Kante $e$ ist nicht im Baum und nicht im Block $I_j$:\\
Flussvariablen bleiben unverändert gleich $0$.

\item Die Kante $e$ ist im Baum, aber nicht im Block $I_j$:\\
Flussvariablen können sich verändern.

\item Die Kante $e$ ist nicht im Baum, aber im Block $I_j$:\\
Flussvariablen können sich verändern.\\
\textit{Spezialfall: Alle Kanten in $I_j$ sind nicht im Baum enthalten.}

\item Die Kante $e$ ist im Baum und im Block $I_j$:\\
Flussvariablen können sich verändern.\\
\textit{Spezialfall: Alle Kanten in $I_j$ sind im Baum enthalten.}
\end{enumerate}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Beispiel}
Startlösung:\\
\begin{minipage}{0.45\linewidth}
\centering
\includegraphics[width= 0.8\linewidth]{\Bilder graph_fuv_1.pdf}
\end{minipage}
\begin{minipage}{0.45\linewidth}
\centering
\includegraphics[width= 0.8\linewidth]{\Bilder graph_fuv_2.pdf}
\end{minipage}
\pause
Optimierung über Block $I = \{\{1,2\}, \{3,4\}\}$:\\
\begin{minipage}{0.45\linewidth}
\centering
\includegraphics[width= 0.8\linewidth]{\Bilder graph_fuv_3.pdf}
\end{minipage}
\begin{minipage}{0.45\linewidth}
\centering
\includegraphics[width= 0.8\linewidth]{\Bilder graph_fuv_4.pdf}
\end{minipage}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Zweite Formulierung: $(\mathcal{MST}_j)(\xi)$}
Menge aller Bögen im Block $I_j$:
\begin{equation*}
A_j^0 := A \cap I_j := \{(u,v) \in V \times V: \{u,v\} \in E \cap I_j\}
\end{equation*}
\pause
Menge der Bögen aus Fall 2:
\begin{equation*}
A_j^1 := \{(u,v) \in V \times V: e=\{u,v\} \in E \cap I_j^C \text{ mit } \xi_e = 1\}
\end{equation*}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Zweite Formulierung: $(\mathcal{MST}_j)(\xi)$}
\small
\begin{alignat*}{3}
&\mbox{min } \quad &\sum_{e \in I_j} c_e x_e\\
&\mbox{s.d. } &\sum_{\substack{(u,v)=a:\\ a \in A_j^0 \cup A_j^1}} f_{uv} - \sum_{\substack{(u,v)=a:\\ a \in A_j^0 \cup A_j^1}} f_{vu} &=
\begin{cases}
|V| - 1 &\text{falls } u = q\\
-1 &\forall u \in V\setminus\{q\}
\end{cases}\\
&&f_{uv} &\leq (|V| - 1) \cdot x_e \quad \forall e= \{u,v\} \in I_j\\ 
&&f_{vu} &\leq (|V| - 1) \cdot x_e \quad \forall e= \{u,v\} \in I_j\\
&&f_{uv} &\leq |V| - 1 \quad \forall (u,v) \in A_j^1\\ 
&&f_{vu} &\leq |V| - 1 \quad \forall (v,u) \in A_j^1\\
&&\sum_{e \in E} x_e &= |V| - 1\\
&&f_{uv} &\geq 0 \quad \forall (u,v) \in A_j^0 \cup A_j^1\\
&&x_e &\in \{0,1\} \quad \forall e \in I_j\\
\end{alignat*}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Aussagen}
\begin{lemma}
$(\mathcal{MST}_j')$ und $(\mathcal{MST}_j)$ sind bis auf den Zielfunktionswert äquivalent und dieser unterscheidet sich genau um $\sum_{e \notin I_j} c_e \xi_e$.
\end{lemma}
\pause
\begin{bem}
$(\mathcal{MST}_j)$ ist \textbf{kein} neues (kleineres) Spannbaum-Problem!\\
Flussvariablen $f_{uv}$ und $f_{vu}$ für die Kante $e = \{u, v\} \notin I_j$ müssen vorhanden sein.
\end{bem}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Beispiel}
blau: \textit{Kanten im Block $I$\\}
rot: \textit{Kanten nicht im Block $I$, aber im Baum\\}
schwarz: \textit{Kanten weder im Block $I$ noch im Baum\\}
\vspace{22pt}
\only<1>{
Kanten im Block $I$ nicht zusammenhängend:\\
\vspace{11pt}
\centering
\includegraphics[height=0.4\textheight]{\Bilder no_mst_1.pdf}\\
}
\only<2-3>{
Spannbaum im Block I möglich:\\
\vspace{11pt}
}
\centering
\only<2>{ \includegraphics[height=0.4\textheight]{\Bilder no_mst_3.pdf}}
\only<3>{ \includegraphics[height=0.4\textheight]{\Bilder no_mst_2.pdf} }
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Dritte Formulierung $(\mathcal{MST}_j^\text{multi})(\xi)$}
\textbf{Ziel: }Ein neues (kleineres) Spannbaum-Problem\\
\vspace{11pt}
\pause
\textbf{Voraussetzung:} Multigraphen (Schleifen und Mehrfachkanten zulassen.\\
\vspace{11pt}
\pause
\footnotesize
\begin{algorithm}[H]
\DontPrintSemicolon
\caption{\small Verkleinern des Graphen für  $(\mathcal{MST}_j^\text{multi})$ }
\begin{enumerate}[<+->]
\item Alle Kanten mit $e\notin I_j$ und $\xi_e = 0$ entfernen.
\item Wähle beliebige Kante $e = \in E \cap I^C$. \\
Es gilt insbesondere $\xi_e = 1$.\\
Falls $E \cap I^C = \emptyset$: Fertig. \label{fuso_start}
\item Fusionieren der beiden Knoten $u$ und $v$ sowie die Kante $e = \{u,v\}$.\\
Merke fusionierte Kante sowie alle gelöschten Kanten (in Reihenfolge).
\item Update von $V$, $E$ und $I^C$.
\item Schleifen können gelöscht werden.
\item Gehe zurück zu~\ref{fuso_start}.
\end{enumerate}
\label{algo:mst_multi}
\end{algorithm}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Beispiel}
\centering
\small
\begin{minipage}{0.45\linewidth}
\centering
Startgraph\\
\includegraphics[height= 0.7\linewidth]{\Bilder multigraph_algo_1.pdf}
\end{minipage}
\pause
\begin{minipage}{0.45\linewidth}
\centering
Entfernen der Kanten gleich 0\\
\includegraphics[height= 0.7\linewidth]{\Bilder multigraph_algo_2.pdf}
\end{minipage}\\
\vspace{22pt}
\pause
\begin{minipage}{0.45\linewidth}
\centering
Fusion der Kante $\{1,4\}$\\
\includegraphics[height= 0.7\linewidth]{\Bilder multigraph_algo_3.pdf}
\end{minipage}
\pause
\begin{minipage}{0.45\linewidth}
\centering
Fusion der Kante $\{14,3\}$\\
\includegraphics[height= 0.7\linewidth]{\Bilder multigraph_algo_4.pdf}
\end{minipage}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Beispiel}
\small
{\centering
\begin{minipage}{0.45\linewidth}
\centering
Startgraph\\[11pt]
\includegraphics[height= 0.9\linewidth]{\Bilder multigraph_algo_0.pdf}
\end{minipage}
\begin{minipage}{0.45\linewidth}
\centering
Endgraph\\[11pt]
\includegraphics[height= 0.9\linewidth]{\Bilder multigraph_algo_5.pdf}
\end{minipage}\\
}
\pause
\vspace{22pt}
\begin{tabular}{lrr}
$(\mathcal{MST}_j')$: & 10 Entscheidungsvariablen, & 20 Flussvariablen\\
$(\mathcal{MST}_j)$: & 6 Entscheidungsvariablen, & 14 Flussvariablen\\
$(\mathcal{MST}_j^\text{multi})$: &4 Entscheidungsvariablen, & 8 Flussvariablen
\end{tabular}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Dritte Formulierung $(\mathcal{MST}_j^\text{multi})(\xi)$}
\small
\begin{alignat*}{3}
&\mbox{min } \quad &\sum_{e \in I_j} c_e x_e\\
&\mbox{s.d. } &\sum_{(u,v) \in A_j^0} f_{uv} - \sum_{(u,v) \in A_j^0} f_{vu} &=
\begin{cases}
|V| - 1 &\text{falls } u = q\\
-1 &\forall u \in V\setminus\{q\}
\end{cases}\\
&&f_{uv} &\leq (|V| - 1) \cdot x_e \quad \forall e= \{u,v\} \in I_j\\ 
&&f_{vu} &\leq (|V| - 1) \cdot x_e \quad \forall e= \{u,v\} \in I_j\\ 
&&\sum_{e \in E} x_e &= |V| - 1\\
&&f_{uv} &\geq 0 \quad \forall (u,v) \in A_j^0\\
&&x_e &\in \{0,1\} \quad \forall e \in I_j\\
\end{alignat*}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Aussagen}
\begin{bem}
Die Schritte müssen bei der Erstellung des Multigraphen $G'$ gespeichert werden, da sonst eine Eins-zu-Eins-Übertragung nicht mehr möglich ist.
\end{bem}
\pause
\begin{lemma}
$(\mathcal{MST}_j)$ und $(\mathcal{MST}_j^\text{multi})$ (nach der Rücktransformation) sind äquivalent.
\end{lemma}
\pause
\underline{Beweisskizze:} Via Induktion.\\
Zeige, dass die Formulierungen vor und nach dem Durchlaufen der der Schleife in Algorithmus~\ref{algo:mst_multi} äquivalent sind.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Rucksack-Problem}
\begin{frame}{Rucksack-Problem}
\small
Erste Formulierung $(\mathcal{KSP}_j')(\xi)$:
\begin{alignat*}{3}
&\mbox{max } \quad &\sum_{i = 1}^n c_i x_i\\
&\mbox{s.d. } &\sum_{i = 1}^n w_i x_i &\leq K\\
&&x_i &\in \{0,1\} \quad \forall i \in I_j\\
&&x_i &=\xi_i \quad \forall i \notin I_j
\end{alignat*}
\pause
Zweite Formulierung $(\mathcal{KSP}_j)(\xi)$:
\begin{alignat*}{3}
&\mbox{max } \quad &\sum_{i \in I_j} c_i x_i\\
&\mbox{s.d. } &\sum_{i \in I_j} w_i x_i &\leq K - \sum_{i \notin I_j} w_i \xi_i\\
&&x_i &\in \{0,1\} \quad \forall i \in I_j
\end{alignat*}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Aussagen}
\begin{lemma}
$(\mathcal{KSP}_j')$ und $(\mathcal{KSP}_j)$ sind bis auf den Zielfunktionswert äquivalent und dieser entscheidet sich genau um $\sum_{i \notin I_j} c_i \xi_i$.
\end{lemma}
\pause
\begin{bem}
$(\mathcal{KSP}_j)$ ist \textbf{ein} neues (kleineres) Rucksack-Problem.
\end{bem}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Wahl der Blöcke}
\begin{frame}{Wahl der Blöcke}
\begin{itemize}[<+->]
\item Es muss gelten: $I_1 \cup I_2 \ldots \cup I_k = \{1, \ldots, n\}$.\\
\item Die Größe der Blöcke kann sowohl Laufzeit als auch Lösung beeinflussen.
\item Die Anzahl der Blöcke kann insbesondere die Laufzeit des Verfahrens beeinflussen.
\end{itemize}
\pause
\vspace{11pt}
Drei Vermutungen:
\begin{enumerate}[<+->]
\item Qualität: Disjunktheit
\item Qualität: Zusammenhang von Größe und Anzahl der Blöcke
\item Dauer: Größe der Blöcke
\end{enumerate}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Reihenfolge der Blöcke}
\begin{lemma}
Gegeben ist eine beliebige Blockeinteilung $I_1, I_2, \ldots, I_k \subseteq \{1, \ldots, n\}$ mit $I_1 \cup I_2 \ldots \cup I_k = \{1, \ldots, n\}$.\\
Betrachte zwei hintereinander liegende Blöcke $I_j$ und $I_{j+1}$. Dann gelten folgenden Aussagen:
\begin{enumerate}
\item Falls $I_j \subseteq I_{j+1}$, dann gilt: Der Zielfunktionswert wird besser oder bleibt zumindest gleich.
\item Falls $I_{j+1} \subseteq I_j$, dann gilt: Der Zielfunktionswert bleibt gleich.
\end{enumerate}
\end{lemma}
\pause
\begin{folg}
Für den zweiten Fall, also wenn $I_{j+1} \subseteq I_j$ gilt, ist die algorithmische Konsequenz, dass der Block $I_{j+1}$ übersprungen werden kann.
\end{folg}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Reihenfolge der Blöcke}
\begin{lemma}\label{verbesserung:induktion}
Gegeben ist eine beliebige Blockeinteilung $I_1, I_2, \ldots, I_k \subseteq \{1, \ldots, n\}$ mit $I_1 \cup I_2 \ldots \cup I_k = \{1, \ldots, n\}$.\\[11pt]
Betrachte aufeinander folgende Blöcke $I_j$ sowie $I_{j+1}, I_{j+2}, \ldots, I_{j+l}$ mit $I_{j+1}, I_{j+2}, \ldots, I_{j+l} \subseteq I_j$. 
Wie sich dabei die Teilmengen $I_{j+1}, I_{j+2}, \ldots, I_{j+l}$ dabei paarweise zueinander verhalten, kann vernachlässigt werden.\\[11pt]
Dann können die Blöcke $I_{j+1}, I_{j+2}, \ldots, I_{j+l}$ übersprungen werden.
\end{lemma}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Umgekehrte Reihenfolge}
Wie wirkt sich die Reihenfolge allgemein aus?\\[11pt]
\pause
\textbf{Deshalb:} Betrachte auch umgekehrte Reihenfolge.\\
\vspace{22pt}
\pause
\textbf{Beispiel: }$I_1 \cup I_2 \cup I_3 = \{1, \ldots, n\}$.\\
Betrachte $[I_1, I_2, I_3]$ als auch $[I_3, I_2, I_1]$.
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Alle k-elementigen Teilmengen}
\begin{overlayarea}{\textwidth}{\textheight}
\vspace{22pt}
\visible<1-5>{
\textbf{Vorteil:} Keine disjunkten Blöcke\\
}
\visible<2-5>{
\textbf{Nachteil:} Sehr viele Blöcke\\
\vspace{22pt}
}
\only<3>{
\centering
\includegraphics[width=0.7\textwidth]{\pics k-el-subs_comparison_nos_3.png}
}
\only<4>{
\centering
\includegraphics[width=0.7\textwidth]{\pics k-el-subs_comparison_nos_4.png}
}
\only<5>{
\begin{nota}
\begin{itemize}
\item $teilmengen(k)$
\item $teilmengen\_rev(k)$
\end{itemize}
\end{nota}
}
\end{overlayarea}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Alle k-elementigen Teilmengen}
\framesubtitle{Spannbaum-Problem}
\footnotesize
\begin{defi}[Matroid]
\begin{itemize}[<+->]
\item $S$ endliche nicht-leere Menge; $I \subseteq P(S)$.\\
$M = (S,I)$ ist genau dann ein Matroid, wenn Folgendes erfüllt ist:
\begin{enumerate}[<+->]
\item $\emptyset \in I$
\item $B \in I$ und $A \subseteq B \quad \Rightarrow \quad A \in I$.
\item $A, B \in I$ und $|A| < |B| \quad \Rightarrow \quad \exists x \in B \setminus A$ mit $A \cup \{x\} \in I$.
\end{enumerate}
\item Eine unabhängige Menge $A \in I$ heißt \textbf{maximal}, wenn $|A| \geq |B|$ für alle $B \in I$ gilt.
\item Ein Matroid M = (S,I) heißt \textbf{gewichtet}, wenn es eine Gewichtsfunktion $c: S \rightarrow \RR^+$ gibt.\\
Für jedes $A \in I$ definiere
\begin{equation*}
c(A) := \sum_{x \in A} c(x)
\end{equation*}
\end{itemize}
\end{defi}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Alle k-elementigen Teilmengen}
\framesubtitle{Spannbaum-Problem}

\begin{framed}
\textbf{Matroid-Optimierungsproblem:} \\
\textbf{Gegeben} sei ein gewichteter Matroid $M=(S,I)$.\\ 
\textbf{Gesucht} ist eine maximale unabhängige Menge mit minimalen Gewicht.
\end{framed}
\pause
\begin{satz}[{\cite[Theorem 12]{schoebel2017}}]
Sei $\mathcal{P}$ ein Matroid-Optimierungsproblem. Seien alle zwei-elementigen Teilmengen in $\mathcal{I}$ enthalten, d.h.
\begin{equation*}
\{ \{ e, e' \}: e,e' \in E \text{ und } e \neq e'\} \subseteq \mathcal{I}.
\end{equation*} 
Dann terminiert das Blockkoordinaten"=Abstiegsverfahren mit einer globalen optimalen Lösung von $\mathcal{P}$.
\end{satz}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Alle k-elementigen Teilmengen}
\framesubtitle{Spannbaum-Problem}
\footnotesize

\begin{satz}
Sei $G=(V,E)$ ein Graph. 
Wähle $ S:= E$ und $I := \{A \, | \, A \subseteq E \text{ und } (V,A) \text{ ist ein Wald}\}$.
Dann ist $(S,I)$ ein Matroid.
\end{satz}
\pause
\begin{lemma}
Betrachte $(\mathcal{MST})$. Gelte wieder
\begin{equation*}
\{ \{ e, e' \}: e,e' \in E \text{ und } e \neq e'\} \subseteq \mathcal{I}
\end{equation*} 
Dann findet das Blockkoordinaten"=Abstiegsverfahren eine optimale Lösung von $(\mathcal{MST})$.
\end{lemma}
\pause
\begin{lemma}
Betrachte $(\mathcal{MST})$.\\
Sei $\mathcal{I}$ die Menge aller k-elementigen Teilmengen, $k \geq 2$, der Kantenmenge $E$ des Graphen.\\
Dann findet das Blockkoordinaten"=Abstiegsverfahren eine optimale Lösung von $(\mathcal{MST})$.
\end{lemma}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Alle k-elementigen Teilmengen}
\framesubtitle{Rucksack-Problem}
Im Allgemeinen wird keine optimale Lösung gefunden.
\end{frame}

\begin{frame}{Disjunkte Blöcke}
\begin{nota}
\begin{itemize}
\item $disjunkt(d)$
\item $disjunkt\_rev(d)$
\end{itemize}
\end{nota}
\vspace{11pt}
\pause
Es gibt $d$ Blöcke mit $\approx \frac{n}{d}$ Elementen.\\
\vspace{11pt}
\pause
\textbf{Beispiel:} 100 Elemente. \\
$disjunkt(3) = \{ \{1, \ldots, 33\}, \{34, \ldots, 66\}, \{ 67, \ldots, 100 \} \}$\\
\vspace{11pt}
\pause
\textbf{Bemerkung:}\\
Das Verfahren terminiert i.A. nicht nach zwei Runden.
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Überlappende Blöcke}
\begin{nota}
\begin{itemize}
\item $ueberlapp(d)$
\item $ueberlapp\_rev(d)$
\end{itemize}
\end{nota}

\vspace{11pt}
\pause
Es gibt $2d-1$ Blöcke mit $\approx \frac{n}{d}$ Elementen.\\
\vspace{11pt}
\pause

\textbf{Beispiel:} 100 Elemente. \\
$ueberlapp(2) = \{ \{1, \ldots 50\}, \{26, \ldots 75\}, \{50, \ldots 100\}, \{76, \ldots 100\} \}$
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Überlappende Blöcke}
\begin{nota}
\begin{itemize}
\item $ueberlapp\_mod(d)$
\item $ueberlapp\_mod\_rev(d)$
\end{itemize}
\end{nota}
\vspace{11pt}
\pause
Es gibt $2d$ Blöcke mit $\approx \frac{n}{d}$ Elementen.\\
\vspace{11pt}
\pause

\textbf{Beispiel:} 100 Elemente. \\
$ueberlapp(2) = \{ \{1, \ldots 50\}, \{26, \ldots 75\}, \{50, \ldots 100\},\{76, \ldots 100\} \cup \{1, \ldots 25\} \}$
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Zufällige Blöcke}
\begin{nota}
\begin{itemize}
\item $zufall(m)$
\end{itemize}
\end{nota}
\pause
\footnotesize
\begin{algorithm}[H]
\DontPrintSemicolon
\caption{Verfahren mit einer zufälligen Blockwahl}
%\KwIn{Test}
%\KwOut{Test2}
Rate eine Startlösung $\xi =: x^{(0)}$.\;
Sei $m \in \NN$ die Blockgröße und $k=0$ der Zähler, wie viele Blöcke benötigt werden.\;
\pause
\Repeat{$f(x^{(k)}) = f((x^{(k-1)})$ {\normalfont \textbf{and}} alle Elemente wurden benutzt}{
	$k \leftarrow k +1$\;
	Erstelle Block $I_k$\;
	Notiere alle im Block $I_k$ benutzen Elemente\;
	Löse $(\mathcal{P}_k)(x^{(k-1)})$\;
	Sei $x^*$ optimal für $(\mathcal{P}_k)(x^{(k-1)})$.\;
	Setze $x^{(k)} = (x^*, \bar{x}_{(k-1)})$.\;
	}
\end{algorithm}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Weitere Wahlmöglichkeiten}
Es gibt viele weitere Möglichkeiten.\\
\pause
\begin{itemize}
\item<2-> Mischwahlen
\item<3-> komplett neue Blockwahlen
\item<4-> auf Probleme/Instanzen angepasste Blockwahlen
\end{itemize}
\vspace{11pt}
\visible<5->{
\textbf{Bemerkung zu Mischwahlen:}\\
Verfahren nur so gut wie das Verfahren mit dem zuerst angewendeten Block.
}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Abbruchbedingungen}
Mögliche Abbruchbedingungen:
\begin{itemize}
\item<2-> Zeit
\item<3-> Anzahl von Runden
\item<4-> Nur noch wenig Veränderung: $\frac{|x_{alt} - x_{neu}|}{x_{neu}} < \epsilon$
\end{itemize}

\visible<5>{
\vspace{11pt}
\textbf{Bemerkung und Frage:}\\
Was passiert, wenn man $x^{(0)} = \xi$ statt $f(x^{(k)}) = f((x^{(k-1)})$ wählt?
}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Kommentar zur Programmierung}
\begin{itemize}
\item<1-> Gurobi
\item<2-> $(\mathcal{MST'})$ und $(\mathcal{KSP'})$
\item<3-> Andere Verfahren für die Teilprobleme verwenden:
\item[]<4-> z.B.: Spannbaum-Problem mit $(\mathcal{MST}_j^\text{multi})$
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Ergebnisse}
{\frame{   \frametitle{Überblick}   \tableofcontents[currentsection]}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Übersicht}

Hauptaspekte:
\begin{itemize}[<+->]
\item Lösungsqualität
\item Anzahl der Runden
\item Zeit
\item Häufigkeit der optimalen Lösung
\end{itemize}

\vspace{22pt}
\visible<5>{
Mittelwert über 10 Instanzen
}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Problem des minimalen Spannbaums}
\textbf{Vollständige Graphen: } $n$ Knoten und $\frac{n \cdot (n-1)}{2}$ Kanten.\\
\vspace{22pt}
\pause
\textbf{"Karge"\, Graphen: } $n$ Knoten und $\lfloor \frac{2}{3} \cdot \frac{n \cdot (n-1)}{2} \rfloor$ Kanten.\\[22pt]
\pause
\textbf{Startlösung: } Zufälliger Spannbaum (immer der gleiche für alle Instanzen)
\end{frame}

\begin{frame}{Rucksack-Problem}
\textbf{Anzahl der Elemente: } $n$\\
\textbf{Kapazität: } $5 \cdot n$\\[22pt]
\pause
\textbf{Startlösung: } Durchlaufe einmal alle Elemente und füge die hinzu, sodass die Summe kleiner oder gleich der Kapazität ist.
\end{frame}


\begin{frame}{Alle k-elementigen Teilmengen bei gleicher Problemgröße}
\small
\only<1>{
\framesubtitle{Spannbaum-Problem}
Anzahl Runden\\[11pt]
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\mst 7_21_all-k-subs_loops.png}\\
Vollständige Graphen
\end{minipage}
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\mst 8_18_all-k-subs_loops.png}\\
Karge Graphen
\end{minipage}
}
\only<2>{
\framesubtitle{Spannbaum-Problem}
Zeit und Anzahl Blöcke\\[11pt]
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\mst 7_21_all-k-subs_time_nos.png}\\
Vollständige Graphen
\end{minipage}
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\mst 8_18_all-k-subs_time_nos.png}\\
Karge Graphen
\end{minipage}
}
\only<3>{
\framesubtitle{Rucksack-Problem}
Anzahl Runden und Häufigkeit der optimalen Lösung\\[11pt]
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\ksp 20_100_all-k-subs_loops.png}\\
\end{minipage}
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\ksp 20_100_all-k-subs_freq2.png}\\
\end{minipage}
}
\only<4>{
\framesubtitle{Rucksack-Problem}
Zeit und Anzahl Blöcke\\[11pt]
\centering
\begin{minipage}{0.7\linewidth}
\centering
\includegraphics[width= \linewidth]{\ksp 20_100_all-k-subs_time_nos.png}\\
\end{minipage}
}
\end{frame}

\begin{frame}{Alle 2-elementige Teilmengen bei unterschiedlichen
Problemgrößen}
\small
\only<1>{
\framesubtitle{Spannbaum-Problem}
Anzahl Runden\\[11pt]
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\mst teilmengen(2)_True_16_44_3_loops.png}\\
Vollständige Graphen
\end{minipage}
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\mst teilmengen(2)_False_16_44_3_loops.png}\\
Karge Graphen
\end{minipage}
}
\only<2>{
\framesubtitle{Spannbaum-Problem}
Zeit\\[11pt]
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\mst teilmengen(2)_True_16_44_3_time.png}\\
Vollständige Graphen
\end{minipage}
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\mst teilmengen(2)_False_16_44_3_time.png}\\
Karge Graphen
\end{minipage}
}
\only<3>{
\framesubtitle{Rucksack-Problem}
Gap der optimalen Lösung\\[11pt]
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\ksp teilmengen(2)_10_100_10_gap.png}
\end{minipage}
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\ksp teilmengen(2)_100_1000_100_gap.png}
\end{minipage}
}
\only<4>{
\framesubtitle{Rucksack-Problem}
Zeit und Anzahl Blöcke\\[11pt]
\centering
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\ksp teilmengen(2)_10_100_10_freq.png}
\end{minipage}
\begin{minipage}{0.49\linewidth}
Bei den Instanzen von 100 bis 1000 Elementen:\\
Optimale Lösung nie getroffen.
\end{minipage}
}
\only<5>{
\framesubtitle{Rucksack-Problem}
Anzahl Runden\\[11pt]
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\ksp teilmengen(2)_10_100_10_loops.png}
\end{minipage}
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\ksp  teilmengen(2)_100_1000_100_loops.png}
\end{minipage}
}
\only<6>{
\framesubtitle{Rucksack-Problem}
Zeit\\[11pt]
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\ksp teilmengen(2)_10_100_10_time.png}
\end{minipage}
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\ksp teilmengen(2)_100_1000_100_time.png}
\end{minipage}
}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Vergleich verschiedener Blockwahlen}
\only<1>{
Spannbaum-Problem mit einen vollständigen Graphen.\\[11pt]
141 Knoten und 9870 Kanten
}
\only<2>{
\framesubtitle{Lösungsqualität}
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\mst 141_9870_comparison_sol.png}
\end{minipage}
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\mst 141_9870_comparison_2_sol.png}
\end{minipage}
}
\only<3>{
\framesubtitle{Anzahl Runden und Zeit}
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\mst 141_9870_comparison_loops.png}
\end{minipage}
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\mst 141_9870_comparison_time.png}
\end{minipage}
}
\only<4>{
\framesubtitle{Vergleich: Zeit und Lösungsqualität}
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\mst 141_9870_time_sol.png}
\end{minipage}
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\mst 141_9870_time_sol_2.png}
\end{minipage}
}
\only<5>{
\framesubtitle{Vergleich: Anzahl Blöcke und Runden}
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\mst 141_9870_nos_loops.png}
\end{minipage}
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\mst 141_9870_nos_loops_2.png}
\end{minipage}
}
\only<6>{
\framesubtitle{Verlauf der Blockwahlen}
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\mst 141_9870_progress_sol.png}
\end{minipage}
\begin{minipage}{0.49\linewidth}
\centering
\includegraphics[width= \linewidth]{\mst 141_9870_progress_gap.png}
\end{minipage}
}
\only<7>{
\framesubtitle{Verlauf bei zufälliger Blockwahl}
\centering
\includegraphics[height= 0.35\textheight]{\mst 141_9870_zufall(100)_0.png}\\
\includegraphics[height= 0.35\textheight]{\mst 141_9870_zufall(1000)_0.png}
}
\end{frame}

\begin{frame}{Vergleich mit kargen Graph und Rucksack-Problem}
Unterschiede:
\begin{itemize}[<+->]
\item Vollständige und karge Graphen: Aussagen lassen sich nicht immer übertragen
\item Rucksack-Problem deutlich schneller
\item bei k-elementigen Teilmengen findet das Rucksack-Problem seltener optimale Lösungen
\end{itemize}
\vspace{11pt}
\visible<4->{Gemeinsamkeiten:}
\begin{itemize}[<+->]
\item Laufzeitverhalten ähnelt sich
\item Lösung für disjunkte Blöcke schlechter
\item Für disjunkte Blöcke terminiert es meist nach zwei Runden
\item $ueberlapp\_mod$ sinnvoller als $ueberlapp$
\item deutlich langsamer als bekannte Algorithmen
\end{itemize}

\end{frame}

\begin{frame}{Die drei Vermutungen}
\begin{enumerate}[<+->]
\item Qualität: Disjunktheit
\item Qualität: Zusammenhang von Größe und Anzahl der Blöcke
\item Dauer: Größe der Blöcke
\end{enumerate}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Schluss}
{\frame{   \frametitle{Überblick}   \tableofcontents[currentsection]}}
\begin{frame}{Schluss und Ausblick}
Viele weitere Aspekte, die noch untersucht werden können.\\
\pause
Insbesondere wie es sich bei komplexeren Problemen verhält.\\
\pause
\vspace{22pt}
\textit{Sei $\mathcal{I}$ eine Auswahl von Teilmengen, für das Verfahren mit einer optimalen Lösung endet. Sei $\mathcal{I'}$ eine Obermenge von $\mathcal{I}$.\\
Endet das Blockkoordinaten-Abstiegsverfahren dann auch mit $\mathcal{I'}$ als Blockwahl mit einer optimalen Lösung?}
\end{frame}

\begin{frame}{Literatur}
\bibliography{literatur} 
\bibliographystyle{alphadin}
\end{frame}

\end{document}
